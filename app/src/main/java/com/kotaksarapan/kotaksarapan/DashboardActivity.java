package com.kotaksarapan.kotaksarapan;

import android.app.DialogFragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.kotaksarapan.kotaksarapan.Helper.SessionManager;
import com.kotaksarapan.kotaksarapan.model.Token.TokenStatusResponse;
import com.kotaksarapan.kotaksarapan.model.balance.BalanceResponse;
import com.kotaksarapan.kotaksarapan.rest.loader.BalanceLoader;
import com.kotaksarapan.kotaksarapan.rest.loader.CheckTokenLoader;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

public class DashboardActivity extends AppCompatActivity implements View.OnClickListener, NavigationView.OnNavigationItemSelectedListener{
    private CircleImageView imageUser;
    private TextView tvTitle;
    private TextView txtDisplayName;
    private TextView txtEmail;
    private TextView txtPhone;

    private Button btnHistory;
    private Button btnFavorites;
    private Button btnAddress;
    private Button btnCredit;
    private ImageButton btnEdit;

    private SessionManager session;
    private CheckTokenLoader checkTokenLoader;
    private BalanceLoader balanceLoader;

    private FragmentTransaction ft ;
    private DialogFragment loading ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        session = new SessionManager(getApplicationContext());

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.setDrawerIndicatorEnabled(true);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        imageUser = (CircleImageView) findViewById(R.id.imageUser);
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        txtDisplayName = (TextView) findViewById(R.id.txtDisplayName);
        txtEmail = (TextView) findViewById(R.id.txtEmail);
        txtPhone = (TextView) findViewById(R.id.txtPhone);
        btnHistory = (Button) findViewById(R.id.btnHistory);
        btnFavorites = (Button) findViewById(R.id.btnFavorites);
        btnAddress = (Button) findViewById(R.id.btnAddress);
        btnCredit = (Button) findViewById(R.id.btnCredit);
        btnEdit = (ImageButton) findViewById(R.id.btnEdit);

        btnEdit.setOnClickListener(this);
        btnHistory.setOnClickListener(this);
        btnFavorites.setOnClickListener(this);
        btnAddress.setOnClickListener(this);
        btnCredit.setOnClickListener(this);
        btnCredit.setText("Credit : ");

        getUserInfo();
        showLoading();

    }

    void showLoading() {
        ft = getFragmentManager().beginTransaction();
        loading = LoadingActivity.newInstance();
        loading.show(ft, "dialog");
    }

    private void getBalance(){
        balanceLoader = new BalanceLoader();
        balanceLoader.load(session.getAuthToken(), new BalanceLoader.BalanceListener() {
            @Override
            public void onComplete(BalanceResponse response) {
                Locale localeID = new Locale("in","ID");
                NumberFormat rupiah = NumberFormat.getInstance(localeID);
                String balance = rupiah.format(response.getData().getAttributes().getBalance());
                btnCredit.setText("Credit : Rp. "+balance+",-");
            }

            @Override
            public void onError(String m) {

            }
        });
    }

    private void getUserInfo(){
        checkTokenLoader = new CheckTokenLoader();
        checkTokenLoader.load(session.getAuthToken(), new CheckTokenLoader.CheckTokenListener() {
            @Override
            public void onComplete(TokenStatusResponse data) {
                loading.dismiss();
                txtDisplayName.setText(data.getUser().getName());
                txtEmail.setText(data.getUser().getEmail());
                txtPhone.setText(data.getUser().getPhone());
                getBalance();
            }

            @Override
            public void onError(String m) {
                try {
                    JSONObject obj = new JSONObject(m);
                    String isActive = obj.getString("isActive");
                    Log.i("TAG", "onError: check token --> "+isActive);
                    if(isActive == "false"){
                        SettingActivity setting = new SettingActivity();
                        setting.doSignOut();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnEdit:
                Toast.makeText(getApplicationContext(),"edit not available",Toast.LENGTH_SHORT).show();
                break;
            case R.id.btnHistory:
                Toast.makeText(getApplicationContext(),"edit not available",Toast.LENGTH_SHORT).show();
                break;
            case R.id.btnFavorites:
                startActivity(new Intent(DashboardActivity.this,FavoritesActivity.class));
                break;
            case R.id.btnAddress:
                startActivity(new Intent(DashboardActivity.this,AddressManagementActivity.class));
                break;
            case R.id.btnCredit:
                startActivity(new Intent(DashboardActivity.this,TopUpActivity.class));
                break;
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            startActivity(new Intent(DashboardActivity.this,MealsDrawerActivity.class));
        } else if (id == R.id.nav_dashboard) {
            if(session.isLoggedIn()){
                startActivity(new Intent(DashboardActivity.this, DashboardActivity.class));
            }else{
                startActivity(new Intent(DashboardActivity.this,MenuLoginActivity.class));
            }
        } else if (id == R.id.nav_favourite) {
            startActivity(new Intent(DashboardActivity.this,FavoritesActivity.class));
        } else if (id == R.id.nav_promotion) {
            startActivity(new Intent(DashboardActivity.this,PromotionsActivity.class));
        } else if (id == R.id.nav_about) {
            startActivity(new Intent(DashboardActivity.this,AboutUsActivity.class));
        } else if (id == R.id.nav_homecook) {
            startActivity(new Intent(DashboardActivity.this,BeHomecookActivity.class));
        } else if (id == R.id.nav_setting) {
            startActivity(new Intent(DashboardActivity.this,SettingActivity.class));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
