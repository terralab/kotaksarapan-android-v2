package com.kotaksarapan.kotaksarapan;

import android.app.DialogFragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.kotaksarapan.kotaksarapan.Helper.AppHelper;
import com.kotaksarapan.kotaksarapan.Helper.MySQLHelper;
import com.kotaksarapan.kotaksarapan.Helper.SessionManager;
import com.kotaksarapan.kotaksarapan.adapter.CategoryPagerAdapter;
import com.kotaksarapan.kotaksarapan.model.Category.CategoryModel;
import com.kotaksarapan.kotaksarapan.model.Category.CategoryResponse;
import com.kotaksarapan.kotaksarapan.model.ListMeals.MealsResponse;
import com.kotaksarapan.kotaksarapan.model.Login.LoginModel;
import com.kotaksarapan.kotaksarapan.model.Login.LoginResponse;
import com.kotaksarapan.kotaksarapan.model.Token.TokenStatusResponse;
import com.kotaksarapan.kotaksarapan.rest.loader.CategoryLoader;
import com.kotaksarapan.kotaksarapan.rest.loader.CheckTokenLoader;
import com.kotaksarapan.kotaksarapan.rest.loader.MealsLoader;
import com.kotaksarapan.kotaksarapan.rest.loader.SearchTitleLoader;
import com.kotaksarapan.kotaksarapan.rest.sender.LoginListener;
import com.kotaksarapan.kotaksarapan.rest.sender.PublicLoginSender;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MealsDrawerActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private CategoryPagerAdapter adapter;
    private MaterialSearchView searchView;

    private CheckTokenLoader checkTokenLoader;
    private SearchTitleLoader searchTitleLoader;

    private CategoryLoader categoryLoader;
    private List<CategoryModel> listCategory = new ArrayList<>();

    private PublicLoginSender sender;
    private LoginModel loginModel;

    private CircleImageView nav_avatar;
    private TextView nav_user;

    private FloatingActionButton fabCart;

    private SessionManager session;
    private String authToken;
    private GoogleApiClient mGoogleApiClient;
    private CallbackManager callbackManager;

    private MySQLHelper db;

    private FragmentTransaction ft ;
    private DialogFragment loading ;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meals_drawer);

        db = MySQLHelper.getInstance(getApplicationContext());

        session = new SessionManager(getApplicationContext());
        authToken = session.getAuthToken();

        Toast.makeText(getApplicationContext(),"status login : "+session.isLoggedIn(),Toast.LENGTH_SHORT).show();

        mGoogleApiClient = AppHelper.getGoogleApiHelper().getGoogleApiClient();

        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.setDrawerIndicatorEnabled(true);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View hView =  navigationView.getHeaderView(0);
        nav_avatar = (CircleImageView) hView.findViewById(R.id.nav_avatar);
        nav_user = (TextView)hView.findViewById(R.id.nav_user);

        fabCart = (FloatingActionButton) findViewById(R.id.fabCart);
        fabCart.setOnClickListener(this);

        viewPager = (ViewPager) findViewById(R.id.vpMeals);
        tabLayout = (TabLayout) findViewById(R.id.tabMeals);
        tabLayout.setupWithViewPager(viewPager);

        searchView = (MaterialSearchView) findViewById(R.id.search_view);
        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(final String query) {
                //Do some magic
                searchTitleLoader = new SearchTitleLoader();
                searchTitleLoader.load(authToken, query, new MealsLoader.MealsListener() {
                    @Override
                    public void onComplete(MealsResponse data) {
                        Log.i("TAG", "onComplete: search complete");
                        Toast.makeText(getApplicationContext(),"search for "+ query,Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(String m) {

                    }
                });

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                //Do some magic
                return false;
            }
        });

        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {
                //Do some magic
            }

            @Override
            public void onSearchViewClosed() {
                //Do some magic
            }
        });

        showLoading();
        if(authToken != null){
            checkToken();
        }else {
            getPublicToken();
        }

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        checkUserLogin();
        checkCart();
    }

    void showLoading() {
        ft = getFragmentManager().beginTransaction();
        loading = LoadingActivity.newInstance();
        loading.show(ft, "dialog");
    }

    public void checkCart(){
        if(db.isEmptyItem()){
            fabCart.setVisibility(View.GONE);
        }else {
            fabCart.setVisibility(View.VISIBLE);
        }
    }

    private void checkToken(){
        checkTokenLoader = new CheckTokenLoader();
        checkTokenLoader.load(authToken, new CheckTokenLoader.CheckTokenListener() {
            @Override
            public void onComplete(TokenStatusResponse data) {
                Log.i("TAG", "onComplete: check token --> "+authToken);
                Log.i("TAG", "onComplete: check token --> "+data.isActive());
                getCategoryTab(authToken);
//                showLoading();
            }

            @Override
            public void onError(String m) {
                try {
                    JSONObject obj = new JSONObject(m);
                    String isActive = obj.getString("isActive");
                    Log.i("TAG", "onError: check token --> "+isActive);
                    if(isActive == "false"){
                        getPublicToken();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void getPublicToken(){
        loginModel = new LoginModel();
        loginModel.setEmail(getResources().getString(R.string.ks_public_email));

        sender = new PublicLoginSender();
        sender.send(loginModel, new LoginListener() {
            @Override
            public void onResponse(LoginResponse response) {
                Log.i("TAG", "onResponse: "+response.isStatus());
                Log.i("TAG", "onResponse: get public token : "+response.getToken());
                session.setAuthToken(response.getToken());
                getCategoryTab(response.getToken());
//                showLoading();
            }

            @Override
            public void onFailure(String m) {

            }
        });
    }

    private void getCategoryTab(String token){
        Log.i("TAG", "getCategoryTab: token get category --> "+token);
        categoryLoader = new CategoryLoader();
        categoryLoader.load(token, new CategoryLoader.CategoryListener() {
            @Override
            public void onComplete(CategoryResponse data) {
                for (int i = 0; i < data.getData().size(); i++) {
                    CategoryModel category= new CategoryModel();
                    Log.i("TAG", "onComplete: id meal"+data.getData().get(i).getId());
                    category.setId(data.getData().get(i).getId());
                    category.setTitle(data.getData().get(i).getAttributes().getTitle());
                    listCategory.add(category);

                    tabLayout.addTab(tabLayout.newTab());
                }

                adapter = new CategoryPagerAdapter(getSupportFragmentManager(),tabLayout.getTabCount(),listCategory);
                viewPager.setAdapter(adapter);
                viewPager.setOffscreenPageLimit(5);

                for (int i = 0; i < data.getData().size();i++) {
                    TextView tab = (TextView) LayoutInflater.from(getBaseContext()).inflate(R.layout.custom_tab, null);
                    tab.setText(data.getData().get(i).getAttributes().getTitle());
                    tab.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_action_rice, 0, 0);
                    tabLayout.getTabAt(i).setCustomView(tab);
                }
                loading.dismiss();
            }

            @Override
            public void onError(String m) {
                Log.i("TAG", "onError: error login --> "+m);

            }
        });
    }

    private void checkUserLogin(){
        if (session.isLoggedIn()){
            HashMap<String, String> user = session.getUserDetails();
            String name = user.get(SessionManager.KEY_NAME);
            String email = user.get(SessionManager.KEY_EMAIL);
            String picture = user.get(SessionManager.KEY_IMAGE_URL);

            nav_user.setText(name);
            Glide.with(this).load(picture).placeholder(R.drawable.placeholder_user).into(nav_avatar);
        }else{
            hideUserDetail();
        }
    }

    private void hideUserDetail(){
        Glide.with(this).load("").placeholder(R.drawable.placeholder_user).into(nav_avatar);
        nav_user.setText("Welcome Guest");
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.search, menu);

        MenuItem item = menu.findItem(R.id.search);
        searchView.setMenuItem(item);

        return true;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            startActivity(new Intent(MealsDrawerActivity.this,MealsDrawerActivity.class));
        } else if (id == R.id.nav_dashboard) {
            if(session.isLoggedIn()){
                startActivity(new Intent(MealsDrawerActivity.this, DashboardActivity.class));
            }else{
            startActivity(new Intent(MealsDrawerActivity.this,MenuLoginActivity.class));
            }
        } else if (id == R.id.nav_favourite) {
            startActivity(new Intent(MealsDrawerActivity.this,FavoritesActivity.class));
        } else if (id == R.id.nav_promotion) {
            startActivity(new Intent(MealsDrawerActivity.this,PromotionsActivity.class));
        } else if (id == R.id.nav_about) {
            startActivity(new Intent(MealsDrawerActivity.this,AboutUsActivity.class));
        } else if (id == R.id.nav_homecook) {
            startActivity(new Intent(MealsDrawerActivity.this,BeHomecookActivity.class));
        } else if (id == R.id.nav_setting) {
            startActivity(new Intent(MealsDrawerActivity.this,SettingActivity.class));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.fabCart:
                startActivity(new Intent(MealsDrawerActivity.this,CartActivity.class));
                break;
        }
    }

    //Google Connection listener
    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
