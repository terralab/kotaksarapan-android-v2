package com.kotaksarapan.kotaksarapan;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import com.kotaksarapan.kotaksarapan.Helper.MySQLHelper;
import com.kotaksarapan.kotaksarapan.Helper.SessionManager;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class DeliveryDateActivity extends AppCompatActivity implements View.OnTouchListener{
    private AutoCompleteTextView txtDay, txtMonth, txtYear = null;
    private FloatingActionButton fabNext;

    private String days[]={"1", "2", "3", "4","5", "6", "7", "8","9", "10", "11", "12",  "13", "14","15", "16", "17", "18","19", "20", "21", "22", "23", "24","25", "26", "27", "28", "29", "30", "31"};
    private String months[]={"Jan", "Feb", "Mar", "Apr","May", "Jun", "Jul", "Aug","Sep", "Oct", "Nov", "Dec"};
    private String years[]={"2017", "2018", "2019"};

    private DateFormat formatDate = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
    private DateFormat formatTime = new SimpleDateFormat("HH:mm", Locale.getDefault());
    private DateFormat formatDateDB = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

    private DateFormat dayFormat = new SimpleDateFormat("dd", Locale.getDefault());
    private DateFormat monthFormat = new SimpleDateFormat("MMM", Locale.getDefault());
    private DateFormat yearFormat = new SimpleDateFormat("yyyy", Locale.getDefault());

    private SessionManager session;
    private MySQLHelper db;
    private String deliveryDate = null;

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_date);

        session = new SessionManager(getApplicationContext());
        db = MySQLHelper.getInstance(getApplicationContext());

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        txtDay = (AutoCompleteTextView) findViewById(R.id.txtDay);
        txtMonth = (AutoCompleteTextView) findViewById(R.id.txtMonth);
        txtYear = (AutoCompleteTextView) findViewById(R.id.txtYear);
        fabNext = (FloatingActionButton) findViewById(R.id.fabNext);

        Cursor c = db.getOrderInfo(session.getID());
        if(c != null){
            c.moveToFirst();
            deliveryDate = c.getString(c.getColumnIndex(MySQLHelper.COLUMN_DELIVERY_DATE));
        }

        if (deliveryDate != null){
            try {
                Date date = formatDateDB.parse(deliveryDate);
                txtDay.setText(dayFormat.format(date));
                txtMonth.setText(monthFormat.format(date));
                txtYear.setText(yearFormat.format(date));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }else {
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DAY_OF_YEAR, 1);
            Date tomorrow = calendar.getTime();

            String day = dayFormat.format(tomorrow);
            String month = monthFormat.format(tomorrow);
            String year = yearFormat.format(tomorrow);

            txtDay.setText(day);
            txtMonth.setText(month);
            txtYear.setText(year);
        }
        validate();

        ArrayAdapter<String> dayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, days);
        ArrayAdapter<String> monthAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, months);
        ArrayAdapter<String> yearAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, years);

        txtDay.setAdapter(dayAdapter);
        txtMonth.setAdapter(monthAdapter);
        txtYear.setAdapter(yearAdapter);

        txtDay.setOnTouchListener(this);
        txtMonth.setOnTouchListener(this);
        txtYear.setOnTouchListener(this);

        txtDay.addTextChangedListener(new DeliveryDateActivity.MyTextWatcher(txtDay));
        txtMonth.addTextChangedListener(new DeliveryDateActivity.MyTextWatcher(txtMonth));
        txtYear.addTextChangedListener(new DeliveryDateActivity.MyTextWatcher(txtYear));

        fabNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Date date = null;
                String strDate = null;
                try {
                    date = formatDate.parse(txtDay.getText()+"-"+txtMonth.getText()+"-"+txtYear.getText());
                    strDate = formatDateDB.format(date);

                    db.updateDate(session.getID(), strDate);
                    startActivity(new Intent(DeliveryDateActivity.this,PaymentActivity.class));
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }
        });

    }

    /* show animation loading screen

    void showAnimationLoading() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        DialogFragment loading = LoadingActivity.newInstance();
        loading.show(ft, "dialog");
    }*/

    public void validate(){
        final String deliv = txtDay.getText()+"-"+txtMonth.getText()+"-"+txtYear.getText();
        final String closeOrder = "18:00";

        //get today date
        Calendar calendar = Calendar.getInstance();
        final Date today = calendar.getTime();
        final String curTime = formatTime.format(today);

        //get tomorrow date
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        final Date tomorrow = calendar.getTime();

        try {
            Date delivDate = formatDate.parse(deliv);
            Date currentTime = formatTime.parse(curTime);
            Date endTime = formatTime.parse(closeOrder);

            if (delivDate.after(today) && currentTime.before(endTime)){
                setActivityBackgroundColor(R.color.colorPrimary);
                fabNext.setVisibility(View.VISIBLE);
            } else if (delivDate.after(tomorrow) && currentTime.after(endTime)){
                setActivityBackgroundColor(R.color.colorPrimary);
                fabNext.setVisibility(View.VISIBLE);
            } else {
                setActivityBackgroundColor(R.color.windowBackground);
                fabNext.setVisibility(View.GONE);
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public void setActivityBackgroundColor(int color) {
        View view = this.getWindow().getDecorView();
        view.setBackgroundColor(getResources().getColor(color));
    }

    private class MyTextWatcher implements TextWatcher {
        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.txtDay:
                    validate();
                    break;
                case R.id.txtMonth:
                    validate();
                    break;
                case R.id.txtYear:
                    validate();
                    break;
            }
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (v.getId()){
            case R.id.txtDay:
                txtDay.showDropDown();
                break;
            case R.id.txtMonth:
                txtMonth.showDropDown();
                break;
            case R.id.txtYear:
                txtYear.showDropDown();
                break;
        }

        return false;
    }
}
