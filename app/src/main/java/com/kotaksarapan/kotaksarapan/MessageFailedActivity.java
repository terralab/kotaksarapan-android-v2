package com.kotaksarapan.kotaksarapan;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MessageFailedActivity extends AppCompatActivity {
    private TextView txtMessageTop;
    private String messageFor;

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_failed);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        messageFor = getIntent().getStringExtra("messageFor");

        txtMessageTop = (TextView) findViewById(R.id.txtMessageTop);

        if (messageFor.equals("registration")) {
            txtMessageTop.setText("REGISTRATION \nUNSUCCESSFULL");
        }else if (messageFor.equals("topup")){
            txtMessageTop.setText("TOP UP UNSUCCESSFULL");
        }else if (messageFor.equals("order")){
            txtMessageTop.setText("ORDER UNSUCCESSFULL");
        }

    }
}
