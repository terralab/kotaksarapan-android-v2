package com.kotaksarapan.kotaksarapan.Helper;

import com.kotaksarapan.kotaksarapan.R;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class AppHelper extends Application {
    private GoogleApiHelper googleApiHelper;
    private static AppHelper mInstance;
    private Context context;

    public void setContext(Context context) {
        this.context = context;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Bariol_Bold.otf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        mInstance = this;
        googleApiHelper = new GoogleApiHelper(mInstance);
    }

    public static synchronized AppHelper getInstance() {
        return mInstance;
    }

    public GoogleApiHelper getGoogleApiHelperInstance() {
        return this.googleApiHelper;
    }
    public static GoogleApiHelper getGoogleApiHelper() {
        return getInstance().getGoogleApiHelperInstance();
    }

}