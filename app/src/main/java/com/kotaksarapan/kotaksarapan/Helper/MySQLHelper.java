package com.kotaksarapan.kotaksarapan.Helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.kotaksarapan.kotaksarapan.model.sqlite.OrderItemModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wresniwahyu on 07/08/2017.
 */

public class MySQLHelper extends SQLiteOpenHelper{
    private static final String DATABASE_NAME = "kotaksarapan.db";
    private static final int DATABASE_VERSION = 1;

    public SQLiteDatabase db;

    public static final String TABLE_ITEMS = "items";
    public static final String TABLE_SIDES = "sides";
    public static final String TABLE_ADDRESS = "address";
    public static final String TABLE_ORDER_INFO = "order_info";

    public static final String COLUMN_COOK_ID = "cookid";
    public static final String COLUMN_MEAL_ID = "mealid";
    public static final String COLUMN_MEAL_NAME = "mealname";
    public static final String COLUMN_PRICE = "price";
    public static final String COLUMN_ORIGINAL_PRICE = "originalprice";
    public static final String COLUMN_AMOUNT = "amount";
    public static final String COLUMN_ORDER_ID = "orderid";
    public static final String COLUMN_ITEM_NOTES = "notes";

    public static final String COLUMN_SIDE_ID = "sideid";
    public static final String COLUMN_SIDE_UUID = "uuid";
    public static final String COLUMN_SIDE_WEIGHT = "weight";
    public static final String COLUMN_ORDER_ITEM_ID = "orderitemid";
    public static final String COLUMN_SIDE_NOTES = "notes";

    public static final String COLUMN_ADDRESS_ID = "addressid";
    public static final String COLUMN_ADDRESS_NAME = "addressname";
    public static final String COLUMN_ADDRESS_LABEL = "addresslabel";
    public static final String COLUMN_ADDRESS = "address";
    public static final String COLUMN_ADDRESS_EXTRA = "addressextra";
    public static final String COLUMN_ADDRESS_LATITUDE = "latitude";
    public static final String COLUMN_ADDRESS_LONGITUDE = "longitude";

    public static final String COLUMN_USER_ID = "userid";
    public static final String COLUMN_FULLNAME = "fullname";
    public static final String COLUMN_MESSAGE = "message";
    public static final String COLUMN_DELIVERY_DATE = "date";
    public static final String COLUMN_COUPON = "couponcode";
    public static final String COLUMN_TOTAL = "total";


    private static MySQLHelper instance = null;

    public static MySQLHelper getInstance(Context context)
    {

        if (instance == null){
            instance = new MySQLHelper(context);
        }
        return instance;
    }

    public MySQLHelper(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String sqlItem = "CREATE TABLE " + TABLE_ITEMS + "(id INTEGER PRIMARY KEY AUTOINCREMENT,"
                + COLUMN_COOK_ID +" TEXT NOT NULL,"
                + COLUMN_MEAL_ID +" TEXT NOT NULL,"
                + COLUMN_MEAL_NAME +" TEXT,"
                + COLUMN_PRICE +" INTEGER,"
                + COLUMN_ORIGINAL_PRICE +" INTEGER,"
                + COLUMN_AMOUNT +" INTEGER,"
                + COLUMN_ITEM_NOTES +" TEXT,"
                + COLUMN_ORDER_ID +" TEXT);";

        String sqlSide = "CREATE TABLE " + TABLE_SIDES + "(id INTEGER PRIMARY KEY AUTOINCREMENT,"
                + COLUMN_SIDE_ID +" INTEGER,"
                + COLUMN_SIDE_UUID +" TEXT,"
                + COLUMN_SIDE_NOTES +" TEXT,"
                + COLUMN_PRICE +" INTEGER,"
                + COLUMN_ORIGINAL_PRICE +" INTEGER,"
                + COLUMN_AMOUNT +" INTEGER,"
                + COLUMN_SIDE_WEIGHT +" INTEGER,"
                + COLUMN_ORDER_ID +" INTEGER,"
                + COLUMN_ORDER_ITEM_ID +" INTEGER);";

        String sqlAddress = "CREATE TABLE " + TABLE_ADDRESS + "(id INTEGER PRIMARY KEY AUTOINCREMENT,"
                + COLUMN_ADDRESS_ID +" INTEGER,"
                + COLUMN_ADDRESS_NAME +" TEXT,"
                + COLUMN_ADDRESS_LABEL +" TEXT,"
                + COLUMN_ADDRESS +" TEXT,"
                + COLUMN_ADDRESS_EXTRA +" TEXT,"
                + COLUMN_ADDRESS_LATITUDE +" TEXT,"
                + COLUMN_ADDRESS_LONGITUDE +" TEXT);";

        String sqlOrder = "CREATE TABLE " + TABLE_ORDER_INFO + "(id INTEGER PRIMARY KEY AUTOINCREMENT,"
                + COLUMN_USER_ID +" INTEGER,"
                + COLUMN_FULLNAME +" TEXT,"
                + COLUMN_ADDRESS_ID +" INTEGER,"
                + COLUMN_ADDRESS_NAME +" TEXT,"
                + COLUMN_ADDRESS +" TEXT,"
                + COLUMN_MESSAGE +" TEXT,"
                + COLUMN_DELIVERY_DATE +" TEXT,"
                + COLUMN_COUPON +" TEXT,"
                + COLUMN_TOTAL +" INTEGER);";

        Log.d("TAG", "onCreate: DB item --> "+sqlItem);
        Log.d("TAG", "onCreate: DB side --> "+sqlSide);
        Log.d("TAG", "onCreate: DB address --> "+sqlAddress);
        Log.d("TAG", "onCreate: DB order --> "+sqlOrder);

        sqLiteDatabase.execSQL(sqlItem);
        sqLiteDatabase.execSQL(sqlSide);
        sqLiteDatabase.execSQL(sqlAddress);
        sqLiteDatabase.execSQL(sqlOrder);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_ITEMS);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_SIDES);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_ADDRESS);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_ORDER_INFO);

        onCreate(sqLiteDatabase);
    }

    public boolean isEmptyItem(){
        db = this.getReadableDatabase();

        boolean empty = true;
        String count = "SELECT COUNT(*) FROM "+ TABLE_ITEMS;
        Cursor cur = db.rawQuery(count, null);
        if (cur != null && cur.moveToFirst()) {
            empty = (cur.getInt (0) == 0);
        }
        cur.close();

        return empty;
    }

    public boolean isEmptyOrderInfo(){
        db = this.getReadableDatabase();

        boolean empty = true;
        String count = "SELECT COUNT(*) FROM "+ TABLE_ORDER_INFO;
        Cursor cur = db.rawQuery(count,null);
        if (cur != null && cur.moveToFirst()){
            empty = (cur.getInt(0) == 0);
        }
        cur.close();

        return empty;
    }

    public boolean isItemExist(String mealID ){
        db = this.getReadableDatabase();

        boolean exist = true;
        String sql = "SELECT * FROM "+ TABLE_ITEMS +" WHERE "+COLUMN_MEAL_ID+" = "+mealID;
        Cursor cursor = db.rawQuery(sql, null);
        if(cursor.getCount() <= 0){
            cursor.close();
            exist = false;
        }
        cursor.close();
        return exist;
    }

    public void emptyTable(){
        db = this.getWritableDatabase();
        db.delete(TABLE_ITEMS, null, null);
        db.delete(TABLE_ORDER_INFO, null, null);
        db.delete(TABLE_ADDRESS, null, null);
        db.close();
    }

    public void emptyAddress(){
        db = this.getWritableDatabase();
        db.delete(TABLE_ADDRESS, null, null);
        db.close();
    }

    public void addTotal(int userID, int total){
        db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_USER_ID, userID);
        values.put(COLUMN_TOTAL, total);
        db.insert(TABLE_ORDER_INFO,null,values);
    }

    public void addItem(String cookID, String mealID, String mealName, int price, int originalPrice, int amount){
        db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_COOK_ID, cookID);
        values.put(COLUMN_MEAL_ID, mealID);
        values.put(COLUMN_MEAL_NAME, mealName);
        values.put(COLUMN_PRICE,price);
        values.put(COLUMN_ORIGINAL_PRICE, originalPrice);
        values.put(COLUMN_AMOUNT,amount);
        db.insert(TABLE_ITEMS,null,values);
    }

    public void addAddress(int address_id, String address_name, String address, double latitude, double longitude){
        db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_ADDRESS_ID, address_id);
        values.put(COLUMN_ADDRESS_NAME, address_name);
        values.put(COLUMN_ADDRESS, address);
        values.put(COLUMN_ADDRESS_LATITUDE, latitude);
        values.put(COLUMN_ADDRESS_LONGITUDE,longitude);
        db.insert(TABLE_ADDRESS,null,values);
    }

    public void updateAddress(int address_id, String address_name, String address, String latitude, String longitude){
        db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_ADDRESS_NAME, address_name);
        values.put(COLUMN_ADDRESS, address);
        values.put(COLUMN_ADDRESS_LATITUDE, latitude);
        values.put(COLUMN_ADDRESS_LONGITUDE,longitude);
        db.update(TABLE_ADDRESS, values, COLUMN_ADDRESS_ID+"="+address_id,null);
    }

    public void updateTotal(int userID, int total){
        db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_TOTAL, total);
        db.update(TABLE_ORDER_INFO, values, COLUMN_USER_ID+"="+userID,null);
    }

    public void updateOrderInfo(int userID, int address_id, String name, String location, String street, String message){
        db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_FULLNAME, name);
        values.put(COLUMN_ADDRESS_ID, address_id);
        values.put(COLUMN_ADDRESS_NAME, location);
        values.put(COLUMN_ADDRESS, street);
        values.put(COLUMN_MESSAGE, message);
        db.update(TABLE_ORDER_INFO, values, COLUMN_USER_ID+" = "+userID, null);
    }

    public void updateDate(int userID, String date){
        db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_DELIVERY_DATE, date);
        db.update(TABLE_ORDER_INFO, values, COLUMN_USER_ID+"="+userID,null);
    }

    public void updateAmount(String mealID, int amount){
        Log.i("TAG", "updateAmount: "+amount);
        db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_AMOUNT, amount);
        db.update(TABLE_ITEMS, values, COLUMN_MEAL_ID+"="+mealID, null);
    }

    public void removeItem(String meal){
        db = this.getWritableDatabase();
        String query = "DELETE FROM " + TABLE_ITEMS + " WHERE " + COLUMN_MEAL_NAME + " = '" + meal + "'";
        db.execSQL(query);
        db.close();
    }

    public int getAddressID(){
        int addressID = 0;
        db = this.getReadableDatabase();
        String sql = "SELECT "+COLUMN_ADDRESS_ID+" FROM "+ TABLE_ADDRESS +";";
        Cursor c = db.rawQuery(sql,null);
        if(c != null && c.moveToFirst()){
            c.moveToFirst();
            addressID = c.getInt(0);
        }
        return addressID;
    }

    public int getOrderAddressID(){
        int addressID = 0;
        db = this.getReadableDatabase();
        String sql = "SELECT "+COLUMN_ADDRESS_ID+" FROM "+ TABLE_ORDER_INFO +";";
        Cursor c = db.rawQuery(sql,null);
        if(c != null && c.moveToFirst()){
            c.moveToFirst();
            addressID = c.getInt(0);
        }
        return addressID;
    }

    public Cursor getOrderInfo(int userID){
        db = this.getReadableDatabase();
        String sql = "SELECT * FROM "+ TABLE_ORDER_INFO +" WHERE "+COLUMN_USER_ID+" = "+userID;
        Cursor cur = db.rawQuery(sql,null);
        return cur;
    }

    public List<OrderItemModel> getAllOrderItem(){
        List<OrderItemModel> data = new ArrayList<>();
        db = this.getReadableDatabase();
        String query = "SELECT * FROM " + TABLE_ITEMS;
        Cursor c = db.rawQuery(query,null);

        if(c.moveToFirst()){
            do{
                OrderItemModel orderItem = new OrderItemModel();
                orderItem.setCookid(c.getString(c.getColumnIndex(COLUMN_COOK_ID)));
                orderItem.setMealid(c.getString(c.getColumnIndex(COLUMN_MEAL_ID)));
                orderItem.setMealname(c.getString(c.getColumnIndex(COLUMN_MEAL_NAME)));
                orderItem.setPrice(c.getString(c.getColumnIndex(COLUMN_PRICE)));
                orderItem.setOriginalprice(c.getString(c.getColumnIndex(COLUMN_ORIGINAL_PRICE)));
                orderItem.setAmount(c.getString(c.getColumnIndex(COLUMN_AMOUNT)));
                data.add(orderItem);
            }while (c.moveToNext());
        }
        c.close();
        return data;
    }

}
