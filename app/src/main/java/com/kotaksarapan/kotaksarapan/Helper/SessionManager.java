package com.kotaksarapan.kotaksarapan.Helper;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.kotaksarapan.kotaksarapan.MealsDrawerActivity;
import com.kotaksarapan.kotaksarapan.MenuLoginActivity;

import java.util.HashMap;

/**
 * Created by wresniwahyu on 20/07/2017.
 */

public class SessionManager {
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;

    int PRIVATE_MODE = 0;

    private static String PREF_NAME = "loginPref";
    private static String IS_LOGIN = "isLoggedIn";

    public static final String KEY_ID = "id";
    public static final String KEY_NAME = "name";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_IMAGE_URL = "image";
    public static final String KEY_AUTH_TOKEN = "authToken";

    public SessionManager(Context context){
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setAuthToken(String authToken){
        editor.putString(KEY_AUTH_TOKEN, authToken);
        editor.commit();
    }

    public String getAuthToken(){
        String token = pref.getString(KEY_AUTH_TOKEN, null);
        return token;
    }

    public void setLogin(int id, String name, String email, String image, String token){
        editor.putBoolean(IS_LOGIN, true);
        editor.putInt(KEY_ID,id);
        editor.putString(KEY_NAME, name);
        editor.putString(KEY_EMAIL, email);
        editor.putString(KEY_IMAGE_URL, image);
        editor.putString(KEY_AUTH_TOKEN, token);
        editor.commit();
    }

    public int getID(){
        int userID = pref.getInt(KEY_ID,0);
        return userID;
    }

    public HashMap<String, String> getUserDetails(){
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(KEY_NAME, pref.getString(KEY_NAME, null));
        user.put(KEY_EMAIL, pref.getString(KEY_EMAIL, null));
        user.put(KEY_IMAGE_URL, pref.getString(KEY_IMAGE_URL, null));
        user.put(KEY_AUTH_TOKEN, pref.getString(KEY_AUTH_TOKEN, null));
        return user;
    }

    public void checkLogin(){
        if(!this.isLoggedIn()){
            // user is not logged in redirect him to Login Activity
            Intent i = new Intent(_context, MenuLoginActivity.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            // Staring Login Activity
            _context.startActivity(i);
        }
    }

    public void logoutUser(){
        editor.clear();
        editor.commit();

        // After logout redirect user to Loing Activity
        Intent i = new Intent(_context, MealsDrawerActivity.class);
        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        // Staring Login Activity
        _context.startActivity(i);
    }

    public boolean isLoggedIn(){
        return pref.getBoolean(IS_LOGIN, false);
    }
}
