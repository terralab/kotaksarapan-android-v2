package com.kotaksarapan.kotaksarapan.Helper;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by wresniwahyu on 4/2/2017.
 */

public class TopUpModel implements Parcelable{
    private float amount;

    public TopUpModel(float amount) {
        this.amount = amount;
    }

    protected TopUpModel(Parcel in) {
        amount = in.readFloat();
    }

    public static final Creator<TopUpModel> CREATOR = new Creator<TopUpModel>() {
        @Override
        public TopUpModel createFromParcel(Parcel in) {
            return new TopUpModel(in);
        }

        @Override
        public TopUpModel[] newArray(int size) {
            return new TopUpModel[size];
        }
    };

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeFloat(amount);
    }
}
