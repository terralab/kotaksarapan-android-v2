package com.kotaksarapan.kotaksarapan;

import android.app.DialogFragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.kotaksarapan.kotaksarapan.Helper.AppHelper;
import com.kotaksarapan.kotaksarapan.Helper.SessionManager;
import com.kotaksarapan.kotaksarapan.model.Login.LoginModel;
import com.kotaksarapan.kotaksarapan.model.Login.LoginProviderModel;
import com.kotaksarapan.kotaksarapan.model.Login.LoginResponse;
import com.kotaksarapan.kotaksarapan.rest.sender.LoginListener;
import com.kotaksarapan.kotaksarapan.rest.sender.LoginProviderSender;
import com.kotaksarapan.kotaksarapan.rest.sender.LoginSender;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private Button btnGoogle, btnFacebook;
    private EditText txtEmailorPhone, txtPassword;
    private TextInputLayout inputEmailorPhone;
    private FloatingActionButton fabNext;
    private TextView tvForgot;

    private SessionManager session;
    private CallbackManager callbackManager;
    private GoogleApiClient mGoogleApiClient;

    private LoginProviderSender providerSender;
    private LoginProviderModel providerModel;

    private LoginSender sender;
    private LoginModel login;

    private int userID;
    private String id;
    private String name;
    private String email;
    private String picture;
    private String token;

    private Boolean valid = false;

    private FragmentTransaction ft ;
    private DialogFragment loading ;

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mGoogleApiClient = AppHelper.getGoogleApiHelper().getGoogleApiClient();

        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        session = new SessionManager(getApplicationContext());

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        btnGoogle = (Button) findViewById(R.id.btnGoogle);
        btnFacebook = (Button) findViewById(R.id.btnFacebook);
        txtEmailorPhone = (EditText) findViewById(R.id.txtEmailorPhone);
        txtPassword = (EditText) findViewById(R.id.txtPassword);
        inputEmailorPhone = (TextInputLayout) findViewById(R.id.inputEmailorPhone);
        fabNext = (FloatingActionButton) findViewById(R.id.fabNext);
        tvForgot = (TextView) findViewById(R.id.tvForgot);

        txtEmailorPhone.addTextChangedListener(new MyTextWatcher(txtEmailorPhone));
        txtPassword.addTextChangedListener(new MyTextWatcher(txtPassword));
        fabNext.setOnClickListener(this);
        tvForgot.setOnClickListener(this);

        btnFacebook.setOnClickListener(this);
        btnFacebook.setEnabled(getString(R.string.facebook_app_id) != "facebook_app_id");

        btnGoogle.setOnClickListener(this);

    }

    private void doProviderLogin(String provider){
        Log.i("login", "onResponse: login provider post --> "+id);
        Log.i("login", "onResponse: login provider post --> "+email);
        Log.i("login", "onResponse: login provider post --> "+name);
        Log.i("login", "onResponse: login provider post --> "+provider);
        Log.i("login", "onResponse: login provider post --> "+session.getAuthToken());

        providerModel = new LoginProviderModel();
        providerModel.setEmail(email);
        providerModel.setName(name);
        providerModel.setProvider(provider);
        providerModel.setProvider_id(id);

        providerSender = new LoginProviderSender();
        providerSender.send(session.getAuthToken(), providerModel, new LoginListener() {
            @Override
            public void onResponse(LoginResponse response) {
                Log.i("login", "onResponse: login provider --> "+response.isStatus());
                Log.i("login", "onResponse: login provider --> "+response.getMessage());
                Log.i("login", "onResponse: login provider --> "+response.getUser().getId());
                Log.i("login", "onResponse: login provider --> "+response.getToken());

                Toast.makeText(getBaseContext(),response.getMessage(),Toast.LENGTH_LONG).show();

                userID = response.getUser().getId();
                token = response.getToken();

                session.setLogin(userID, name, email, picture, token);
//                session.setAuthToken(token);

                startActivity(new Intent(LoginActivity.this, MealsDrawerActivity.class));
                finish();
            }

            @Override
            public void onFailure(String m) {

            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 0) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    //sign in via Google
    private void doSignInGoogle(){
        Intent googleSignIn = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(googleSignIn, 0);
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d("TAG", "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.

            GoogleSignInAccount acct = result.getSignInAccount();
//            gToken = acct.getIdToken();
            name = acct.getDisplayName();
            email = acct.getEmail();
            picture = acct.getPhotoUrl().toString();
            id = acct.getId();

//            Toast.makeText(this,"Hello "+name,Toast.LENGTH_SHORT).show();
            doProviderLogin("google");
        } else {
            // Signed out, show unauthenticated UI.
            Log.i("TAG", "handleSignInResult: token is null");
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
    //end of sign in via Google

    //sign in via facebook
    private void doSignInFacebook(){
        LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, Arrays.asList("email"));
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                new GetFbUserInfo(loginResult).execute();

            }

            @Override
            public void onCancel() {
                Toast.makeText(LoginActivity.this, "Facebook login cancelled",
                        Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(LoginActivity.this, "Error in Facebook login " +
                        error.getMessage(), Toast.LENGTH_LONG).show();
//                        LoginManager.getInstance().logOut();
            }
        });
    }

    private class GetFbUserInfo extends AsyncTask<Void, Void, String> {
        private final LoginResult loginResult;
//        private ProgressDialog dialog;

        public GetFbUserInfo(LoginResult loginResult) {
            this.loginResult = loginResult;
        }

        @Override
        protected void onPreExecute() {
//            dialog = ProgressDialog.show(LoginActivity.this, "Wait", "Getting user name");
            showLoading();
        }

        @Override
        protected String doInBackground(Void... params) {
            final GraphRequest request = GraphRequest.newMeRequest(
                    loginResult.getAccessToken(),
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(JSONObject object, GraphResponse response) {
                            // Application code
                            Log.v("LoginActivity", response.toString());
                        }
                    });
            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,name,email,picture{url}");
            request.setParameters(parameters);
            GraphResponse graphResponse = request.executeAndWait();
            try {
                id = graphResponse.getJSONObject().getString("id");
                name = graphResponse.getJSONObject().getString("name");
                email = graphResponse.getJSONObject().getString("email");
                picture = graphResponse.getJSONObject().getJSONObject("picture").getJSONObject("data").getString("url");
//                fbToken = loginResult.getAccessToken().getToken();

                return name;
            } catch (JSONException e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(String response) {
            loading.dismiss();
            if (response != null) {
                Toast.makeText(LoginActivity.this, "Hello " + response, Toast.LENGTH_SHORT).show();
                doProviderLogin("facebook");
            } else {
                Toast.makeText(LoginActivity.this, "Unable to get user name from Facebook",Toast.LENGTH_LONG).show();
            }
        }
    }
    //end of sign in via facebook

    //sign in via apps kotak sarapan
    private void doSignIn(){
        login = new LoginModel();
        login.setEmail(txtEmailorPhone.getText().toString());
        login.setPassword(txtPassword.getText().toString());

        sender = new LoginSender();
        sender.send(session.getAuthToken(),login, new LoginListener() {
            @Override
            public void onResponse(LoginResponse response) {
                Log.i("TAG", "onResponse: get token app --> "+response.getToken());
                Log.i("TAG", "onResponse: login userid --> "+response.getUser().getId());

                userID = response.getUser().getId();
                name = response.getUser().getName();
                email = response.getUser().getEmail();
                picture = "";
                token = response.getToken();

                session.setLogin(userID, name, email, picture, token);
//                session.setAuthToken(token);

                startActivity(new Intent(LoginActivity.this, MealsDrawerActivity.class));

            }

            @Override
            public void onFailure(String m) {
                try {
                    JSONObject obj = new JSONObject(m);
                    String error = obj.getString("error");
                    Toast.makeText(getApplicationContext(),error,Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
    //end of sign in via apps kotaksarapan

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnGoogle:
                doSignInGoogle();
                break;
            case R.id.btnFacebook:
                doSignInFacebook();
                break;
            case R.id.fabNext:
                doSignIn();
                break;
            case R.id.tvForgot:
                Toast.makeText(this,"Forgot Password Activity",Toast.LENGTH_SHORT).show();
                break;
        }
    }

    void showLoading() {
        ft = getFragmentManager().beginTransaction();
        loading = LoadingActivity.newInstance();
        loading.show(ft, "dialog");
    }

    public void setActivityBackgroundColor(int color) {
        View view = this.getWindow().getDecorView();
        view.setBackgroundColor(color);
    }

    private boolean validateEmailorPhone() {
        String email = txtEmailorPhone.getText().toString().trim();

        if (email.isEmpty() || !isValidEmail(email)) {
            valid = false;
            inputEmailorPhone.setError("email/phone number invalid!");
            requestFocus(txtEmailorPhone);
            return false;
        } else {
            valid = true;
            inputEmailorPhone.setErrorEnabled(false);
        }

        return true;
    }

    private static boolean isValidEmail(String emailorphone) {
        return !TextUtils.isEmpty(emailorphone) && android.util.Patterns.EMAIL_ADDRESS.matcher(emailorphone).matches() || android.util.Patterns.PHONE.matcher(emailorphone).matches();
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void validateComplete(){
        if (!txtEmailorPhone.getText().toString().isEmpty() && !txtPassword.getText().toString().isEmpty() && valid){
            setActivityBackgroundColor(0xfffec713);
            fabNext.setVisibility(View.VISIBLE);
        }else{
            setActivityBackgroundColor(0xfffbfbfb);
            fabNext.setVisibility(View.GONE);
        }
    }

    private class MyTextWatcher implements TextWatcher {
        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.txtEmailorPhone:
                    validateEmailorPhone();
                    validateComplete();
                    break;
                case R.id.txtPassword:
                    validateComplete();
                    break;
            }
        }
    }

}
