package com.kotaksarapan.kotaksarapan;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.kotaksarapan.kotaksarapan.Helper.MySQLHelper;
import com.kotaksarapan.kotaksarapan.Helper.SessionManager;
import com.kotaksarapan.kotaksarapan.adapter.MealAdapter;
import com.kotaksarapan.kotaksarapan.interfaces.OnCartClick;
import com.kotaksarapan.kotaksarapan.model.ListMeals.DataModel;
import com.kotaksarapan.kotaksarapan.model.ListMeals.MealsResponse;
import com.kotaksarapan.kotaksarapan.model.Meal.MealModel;
import com.kotaksarapan.kotaksarapan.rest.loader.FavoriteLoader;
import com.kotaksarapan.kotaksarapan.rest.loader.MealsLoader;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class FavoritesActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, MealsLoader.MealsListener, OnCartClick {
    private LinearLayoutManager manager;
    private RecyclerView rvMeals;
    private MealAdapter adapter;
    private List<MealModel> listMeal;

    private FloatingActionButton fabCart;

    private FavoriteLoader loader;

    private SessionManager session;
    private MySQLHelper db;

    private MaterialSearchView searchView;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorites);

        session = new SessionManager(getApplicationContext());
        db = MySQLHelper.getInstance(getApplicationContext());

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.setDrawerIndicatorEnabled(true);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        fabCart = (FloatingActionButton) findViewById(R.id.fabCart);

        searchView = (MaterialSearchView) findViewById(R.id.search_view);
        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                //Do some magic
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                //Do some magic
                return false;
            }
        });

        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {
                //Do some magic
            }

            @Override
            public void onSearchViewClosed() {
                //Do some magic
            }
        });

        listMeal = new ArrayList<>();
        adapter = new MealAdapter(listMeal, this, this);

        rvMeals = (RecyclerView) findViewById(R.id.rvMeals);
        manager = new LinearLayoutManager(this);
        rvMeals.setLayoutManager(manager);
        rvMeals.setAdapter(adapter);

        loader = new FavoriteLoader();
        loader.load(session.getAuthToken(),this);

        checkCart();
    }

    public void checkCart(){
        if(db.isEmptyItem()){
            fabCart.setVisibility(View.GONE);
        }else {
            fabCart.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MaterialSearchView.REQUEST_VOICE && resultCode == RESULT_OK) {
            ArrayList<String> matches = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            if (matches != null && matches.size() > 0) {
                String searchWrd = matches.get(0);
                if (!TextUtils.isEmpty(searchWrd)) {
                    searchView.setQuery(searchWrd, false);
                }
            }

            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search, menu);

        MenuItem item = menu.findItem(R.id.search);
        searchView.setMenuItem(item);

        return true;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            startActivity(new Intent(FavoritesActivity.this,MealsDrawerActivity.class));
        } else if (id == R.id.nav_dashboard) {
            if(session.isLoggedIn()){
                startActivity(new Intent(FavoritesActivity.this, DashboardActivity.class));
            }else{
                startActivity(new Intent(FavoritesActivity.this,MenuLoginActivity.class));
            }
        } else if (id == R.id.nav_favourite) {
            startActivity(new Intent(FavoritesActivity.this,FavoritesActivity.class));
        } else if (id == R.id.nav_promotion) {
            startActivity(new Intent(FavoritesActivity.this,PromotionsActivity.class));
        } else if (id == R.id.nav_about) {
            startActivity(new Intent(FavoritesActivity.this,AboutUsActivity.class));
        } else if (id == R.id.nav_homecook) {
            startActivity(new Intent(FavoritesActivity.this,BeHomecookActivity.class));
        } else if (id == R.id.nav_setting) {
            startActivity(new Intent(FavoritesActivity.this,SettingActivity.class));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onComplete(MealsResponse data) {
        Log.i("TAG", "onComplete favorite");
        for (DataModel dat : data.getData())
        {
            Log.i("TAG", "onComplete: "+dat.getAttributes().getTitle()+" is favourite --> "+dat.getAttributes().isfavorite());
            MealModel meal = new MealModel();
            meal.setId(dat.getId());
            meal.set_cookid(dat.getAttributes().get_cookid());
            meal.setTitle(dat.getAttributes().getTitle());
            meal.setImagesmall(dat.getAttributes().getImagesmall());
            meal.setImagecook(dat.getAttributes().getCook().getImagecook());
            meal.setSummary(dat.getAttributes().getSummary());
            meal.setPrice(dat.getAttributes().getPrice());
            meal.setRating(dat.getAttributes().getRating());
//            meal.setCategoryId(dat.getAttributes().getCategoryId());
            meal.set_cookid(dat.getAttributes().get_cookid());
            meal.setIsfavorite(dat.getAttributes().isfavorite());
            meal.setIsavailable(dat.getAttributes().isavailable());
            meal.setFullname(dat.getAttributes().getCook().getFullname());
            meal.setNickname(dat.getAttributes().getCook().getNickname());
            listMeal.add(meal);
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onError(String m) {

    }

    @Override
    public void onClickCart(String cookID, String mealID, String mealName, int price, int originalPrice, int amount) {
        if(db.isItemExist(String.valueOf(mealID))){
            Toast.makeText(getApplicationContext(),"already in cart",Toast.LENGTH_SHORT).show();
        }else {
            db.addItem(String.valueOf(cookID), String.valueOf(mealID), mealName, price, originalPrice, amount);
            checkCart();
        }
    }
}
