package com.kotaksarapan.kotaksarapan;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.kotaksarapan.kotaksarapan.Helper.MySQLHelper;
import com.kotaksarapan.kotaksarapan.Helper.SessionManager;
import com.kotaksarapan.kotaksarapan.interfaces.AddressListener;
import com.kotaksarapan.kotaksarapan.interfaces.AddressesListener;
import com.kotaksarapan.kotaksarapan.model.address.AddressModel;
import com.kotaksarapan.kotaksarapan.model.address.AddressResponse;
import com.kotaksarapan.kotaksarapan.model.address.AddressesResponse;
import com.kotaksarapan.kotaksarapan.rest.loader.UserAddressLoader;
import com.kotaksarapan.kotaksarapan.rest.sender.CreateAddressSender;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class DeliveryAddressActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {
    private EditText txtFullName;
    private EditText txtLocation;
    private EditText txtStreet;
    private EditText txtMessage;
    private FloatingActionButton fabNext;

    private UserAddressLoader loader;

    private SessionManager session;
    private MySQLHelper db;

    private int address_id = 0;
    private String address_name = "";
    private String address = "";
    private double latitude = 0;
    private double longitude = 0;
    private String fullname = "";
    private String message = "";

    private int PLACE_PICKER_REQUEST = 1;

    private GoogleApiClient mGoogleApiClient;

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onStart() {
        super.onStart();
        if( mGoogleApiClient != null )
            mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        if( mGoogleApiClient != null && mGoogleApiClient.isConnected() ) {
            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_address);

        session = new SessionManager(getApplicationContext());
        db = MySQLHelper.getInstance(getApplicationContext());

        mGoogleApiClient = new GoogleApiClient
                .Builder( this )
                .enableAutoManage( this, 0,  this)
                .addApi( Places.GEO_DATA_API )
                .addApi( Places.PLACE_DETECTION_API )
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener( this)
                .build();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        txtFullName = (EditText) findViewById(R.id.txtFullName);
        txtLocation = (EditText) findViewById(R.id.txtLocation);
        txtStreet = (EditText) findViewById(R.id.txtStreet);
        txtMessage = (EditText) findViewById(R.id.txtMessage);

        txtFullName.addTextChangedListener(new MyTextWatcher(txtFullName));
        txtMessage.addTextChangedListener(new MyTextWatcher(txtMessage));

        fabNext = (FloatingActionButton) findViewById(R.id.fabNext);

        txtLocation.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN) {
                    displayPlacePicker();
                }
                return true;
            }
        });

        fabNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final int id = session.getID();
                fullname = txtFullName.getText().toString();
                address_name = txtLocation.getText().toString();
                address = txtStreet.getText().toString();
                message = txtMessage.getText().toString();

                if(db.getAddressID() == 0){
                    saveAddress();
                }

                db.updateOrderInfo(id, address_id, fullname, address_name, address, message);
                startActivity(new Intent(DeliveryAddressActivity.this, DeliveryDateActivity.class));
            }
        });

        getAddress();
    }

    private void validate(){
        if(!txtFullName.getText().toString().trim().isEmpty() && !txtLocation.getText().toString().trim().isEmpty() && !txtStreet.getText().toString().trim().isEmpty() && !txtMessage.getText().toString().trim().isEmpty()){
            fabNext.setVisibility(View.VISIBLE);
            setActivityBackgroundColor(R.color.colorPrimary);
        }else {
            fabNext.setVisibility(View.GONE);
            setActivityBackgroundColor(R.color.windowBackground);
        }
    }

    private void saveAddress(){
        AddressModel address = new AddressModel();
        address.setName(txtLocation.getText().toString().trim());
        address.setAddress(txtStreet.getText().toString().trim());
        address.setLabel(txtLocation.getText().toString().trim());
        address.setLatitude(String.valueOf(latitude));
        address.setLongitude(String.valueOf(longitude));

        CreateAddressSender sender = new CreateAddressSender();
        sender.send(session.getAuthToken(), address, new AddressListener() {
            @Override
            public void onResponse(AddressResponse response) {
                String label = response.getData().getAttributes().getLabel();
                Toast.makeText(getApplicationContext(),"address "+label+" added",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(String m) {

            }
        });
    }

    private void getAddress(){
        loader = new UserAddressLoader();
        loader.load(session.getAuthToken(), new AddressesListener() {
            @Override
            public void onResponse(AddressesResponse response) {
                address_id = response.getData().get(0).getId();
                address_name = response.getData().get(0).getAttributes().getName();
                address = response.getData().get(0).getAttributes().getAddress();

                if(db.getOrderAddressID() != address_id){
                    String name = session.getUserDetails().get(SessionManager.KEY_NAME);
                    txtFullName.setText(name);
                    txtLocation.setText(address_name);
                    txtStreet.setText(address);
                }else{
                    checkDB();
                }

                if(db.getAddressID() != address_id){
                    db.emptyAddress();
                    db.addAddress(address_id, address_name, address, latitude, longitude);
                }
            }

            @Override
            public void onError(String m) {
                String name = session.getUserDetails().get(SessionManager.KEY_NAME);
                txtFullName.setText(name);
            }
        });
    }

    private void checkDB(){
            Cursor c = db.getOrderInfo(session.getID());
            if(c != null){
                c.moveToFirst();
                fullname = c.getString(c.getColumnIndex(MySQLHelper.COLUMN_FULLNAME));
                address_name = c.getString(c.getColumnIndex(MySQLHelper.COLUMN_ADDRESS_NAME));
                address = c.getString(c.getColumnIndex(MySQLHelper.COLUMN_ADDRESS));
                message = c.getString(c.getColumnIndex(MySQLHelper.COLUMN_MESSAGE));

                txtFullName.setText(fullname);
                txtLocation.setText(address_name);
                txtStreet.setText(address);
                txtMessage.setText(message);
                c.close();
        }
    }

    private void displayPlacePicker() {
        if( mGoogleApiClient == null || !mGoogleApiClient.isConnected() )
            return;

        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

        try {
            startActivityForResult( builder.build( this ), PLACE_PICKER_REQUEST );
        } catch ( GooglePlayServicesRepairableException e ) {
            Log.d( "PlacesAPI Demo", "GooglePlayServicesRepairableException thrown" );
        } catch ( GooglePlayServicesNotAvailableException e ) {
            Log.d( "PlacesAPI Demo", "GooglePlayServicesNotAvailableException thrown" );
        }
    }

    public void setActivityBackgroundColor(int color) {
        View view = this.getWindow().getDecorView();
        view.setBackgroundColor(getResources().getColor(color));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);

                if(place.getAddress().toString().contains("Jakarta")) {
                    address_name = place.getName().toString();
                    address = place.getAddress().toString();
                    latitude = place.getLatLng().latitude;
                    longitude = place.getLatLng().longitude;
                    txtLocation.setText(address_name);
                    txtStreet.setText(address);
                    validate();
                }else{
                    txtLocation.setText(null);
                    txtStreet.setText(null);
                    validate();
                    Toast.makeText(getApplicationContext(),"this service is only available in Jakarta",Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.i("TAG", "onConnectionFailed: ");
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.i("TAG", "onConnected: ");
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i("TAG", "onConnectionSuspended: ");
    }


    private class MyTextWatcher implements TextWatcher {
        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.txtFullName:
                    validate();
                    break;
                case R.id.txtMessage:
                    validate();
                    break;
            }
        }
    }

}
