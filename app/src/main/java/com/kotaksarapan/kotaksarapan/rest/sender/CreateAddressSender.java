package com.kotaksarapan.kotaksarapan.rest.sender;

import com.kotaksarapan.kotaksarapan.interfaces.AddressListener;
import com.kotaksarapan.kotaksarapan.model.address.AddressModel;
import com.kotaksarapan.kotaksarapan.model.address.AddressResponse;
import com.kotaksarapan.kotaksarapan.rest.ASRetrofitRequest;
import com.kotaksarapan.kotaksarapan.rest.ASRetrofitRequestBuilder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by wresniwahyu on 19/09/2017.
 */

public class CreateAddressSender extends ASRetrofitRequestBuilder {
    public void send(String token, AddressModel model, final AddressListener listener){
        ASRetrofitRequest request = ASRetrofitRequestBuilder.createService(ASRetrofitRequest.class,token);
        Call<AddressResponse> callback = request.sendAddress(model);
        callback.enqueue(new Callback<AddressResponse>() {
            @Override
            public void onResponse(Call<AddressResponse> call, Response<AddressResponse> response) {
                listener.onResponse(response.body());
            }

            @Override
            public void onFailure(Call<AddressResponse> call, Throwable t) {
                listener.onFailure(t.getMessage());
            }
        });
    }
}
