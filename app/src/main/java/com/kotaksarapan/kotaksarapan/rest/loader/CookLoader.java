package com.kotaksarapan.kotaksarapan.rest.loader;

import com.kotaksarapan.kotaksarapan.model.Cook.CookResponse;
import com.kotaksarapan.kotaksarapan.rest.ASRetrofitRequest;
import com.kotaksarapan.kotaksarapan.rest.ASRetrofitRequestBuilder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by wresniwahyu on 6/5/2017.
 */

public class CookLoader extends ASRetrofitRequestBuilder{
    public interface CookListener{
        void onComplete(CookResponse data);
        void onError(String m);
    }

    public void load(String token, int id, final CookListener listener){
        ASRetrofitRequest request = ASRetrofitRequestBuilder.createService(ASRetrofitRequest.class, token);
        Call<CookResponse> count = request.getCook(id);
        count.enqueue(new Callback<CookResponse>() {
            @Override
            public void onResponse(Call<CookResponse> call, Response<CookResponse> response) {
                listener.onComplete(response.body());
            }

            @Override
            public void onFailure(Call<CookResponse> call, Throwable t) {
                listener.onError(t.getMessage());
            }
        });
    }

}
