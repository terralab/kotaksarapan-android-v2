package com.kotaksarapan.kotaksarapan.rest.loader;

import com.kotaksarapan.kotaksarapan.interfaces.AddressesListener;
import com.kotaksarapan.kotaksarapan.model.address.AddressesResponse;
import com.kotaksarapan.kotaksarapan.rest.ASRetrofitRequest;
import com.kotaksarapan.kotaksarapan.rest.ASRetrofitRequestBuilder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by wresniwahyu on 20/09/2017.
 */

public class UserAddressLoader extends ASRetrofitRequestBuilder {
    public void load(String token, final AddressesListener listener){
        ASRetrofitRequest request = ASRetrofitRequestBuilder.createService(ASRetrofitRequest.class,token);
        Call<AddressesResponse> count = request.getAddresses();
        count.enqueue(new Callback<AddressesResponse>() {
            @Override
            public void onResponse(Call<AddressesResponse> call, Response<AddressesResponse> response) {
                listener.onResponse(response.body());
            }

            @Override
            public void onFailure(Call<AddressesResponse> call, Throwable t) {
                listener.onError(t.getMessage());
            }
        });
    }
}
