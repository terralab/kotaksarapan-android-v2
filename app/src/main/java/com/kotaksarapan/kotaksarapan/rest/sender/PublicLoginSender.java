package com.kotaksarapan.kotaksarapan.rest.sender;

import android.util.Log;

import com.kotaksarapan.kotaksarapan.model.Login.LoginModel;
import com.kotaksarapan.kotaksarapan.model.Login.LoginResponse;
import com.kotaksarapan.kotaksarapan.rest.ASRetrofitRequest;
import com.kotaksarapan.kotaksarapan.rest.ASRetrofitRequestBuilder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by wresniwahyu on 25/07/2017.
 */

public class PublicLoginSender extends ASRetrofitRequestBuilder {
    public void send(LoginModel model, final LoginListener listener){
        ASRetrofitRequest request = ASRetrofitRequestBuilder.createService(ASRetrofitRequest.class);
        Call<LoginResponse> callback = request.sendPublicLogin(model);
        callback.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                listener.onResponse(response.body());
                Log.i("TAG", "onResponse: response body--> "+response.body());
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                listener.onFailure(t.getMessage());
            }
        });
    }
}
