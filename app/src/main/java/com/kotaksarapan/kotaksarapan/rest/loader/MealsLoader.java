package com.kotaksarapan.kotaksarapan.rest.loader;

import com.kotaksarapan.kotaksarapan.model.ListMeals.MealsResponse;
import com.kotaksarapan.kotaksarapan.rest.ASRetrofitRequest;
import com.kotaksarapan.kotaksarapan.rest.ASRetrofitRequestBuilder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by wresniwahyu on 5/18/2017.
 */

public class MealsLoader extends ASRetrofitRequestBuilder {
    public interface MealsListener {
        void onComplete(MealsResponse data);
        void onError(String m);
    }

    public void load(String token, int id, int page, final MealsListener listener) {
        ASRetrofitRequest request = ASRetrofitRequestBuilder.createService(ASRetrofitRequest.class, token);
        Call<MealsResponse> count = request.getMeals(id, page);
        count.enqueue(new Callback<MealsResponse>() {
            @Override
            public void onResponse(Call<MealsResponse> call, Response<MealsResponse> response) {
                listener.onComplete(response.body());
            }

            @Override
            public void onFailure(Call<MealsResponse> call, Throwable t) {
                listener.onError(t.getMessage());
            }
        });
    }
}
