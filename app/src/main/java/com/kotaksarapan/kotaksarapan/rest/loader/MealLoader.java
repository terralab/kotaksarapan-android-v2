package com.kotaksarapan.kotaksarapan.rest.loader;

import com.kotaksarapan.kotaksarapan.model.ListMeals.MealsResponse;
import com.kotaksarapan.kotaksarapan.model.Meal.MealResponse;
import com.kotaksarapan.kotaksarapan.rest.ASRetrofitRequest;
import com.kotaksarapan.kotaksarapan.rest.ASRetrofitRequestBuilder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by wresniwahyu on 5/31/2017.
 */

public class MealLoader extends ASRetrofitRequestBuilder {
    public interface MealListener {
        void onComplete(MealResponse data);
        void onError(String m);
    }

    public void load(String token, int id, final MealListener listener){
        ASRetrofitRequest request = ASRetrofitRequestBuilder.createService(ASRetrofitRequest.class, token);
        Call<MealResponse> count = request.getMeal(id);
        count.enqueue(new Callback<MealResponse>() {
            @Override
            public void onResponse(Call<MealResponse> call, Response<MealResponse> response) {
                listener.onComplete(response.body());
            }

            @Override
            public void onFailure(Call<MealResponse> call, Throwable t) {
                listener.onError(t.getMessage());
            }
        });
    }
}
