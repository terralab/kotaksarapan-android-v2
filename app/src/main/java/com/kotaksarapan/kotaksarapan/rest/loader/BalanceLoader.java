package com.kotaksarapan.kotaksarapan.rest.loader;

import com.kotaksarapan.kotaksarapan.model.balance.BalanceResponse;
import com.kotaksarapan.kotaksarapan.rest.ASRetrofitRequest;
import com.kotaksarapan.kotaksarapan.rest.ASRetrofitRequestBuilder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by wresniwahyu on 18/09/2017.
 */

public class BalanceLoader extends ASRetrofitRequestBuilder{
    public interface BalanceListener{
        void onComplete(BalanceResponse response);
        void onError(String m);
    }

    public void load(String token, final BalanceListener listener){
        final ASRetrofitRequest request =  ASRetrofitRequestBuilder.createService(ASRetrofitRequest.class, token);
        Call<BalanceResponse> count = request.getBalance();
        count.enqueue(new Callback<BalanceResponse>() {
            @Override
            public void onResponse(Call<BalanceResponse> call, Response<BalanceResponse> response) {
                listener.onComplete(response.body());
            }

            @Override
            public void onFailure(Call<BalanceResponse> call, Throwable t) {
                listener.onError(t.getMessage());
            }
        });

    }
}
