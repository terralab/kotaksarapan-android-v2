package com.kotaksarapan.kotaksarapan.rest.sender;

import android.util.Log;

import com.kotaksarapan.kotaksarapan.model.Login.LoginModel;
import com.kotaksarapan.kotaksarapan.model.Login.LoginResponse;
import com.kotaksarapan.kotaksarapan.rest.ASRetrofitRequest;
import com.kotaksarapan.kotaksarapan.rest.ASRetrofitRequestBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by wresniwahyu on 24/07/2017.
 */

public class LoginSender extends ASRetrofitRequestBuilder{
    public void send(String token, LoginModel model, final LoginListener listener){
        ASRetrofitRequest request = ASRetrofitRequestBuilder.createService(ASRetrofitRequest.class, token);
        Call<LoginResponse> callback = request.sendLogin(model);
        callback.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if(response.body() == null){
                    try {
                        JSONObject obj = new JSONObject(response.errorBody().string());
                        Log.i("TAG", "onResponse: "+obj.toString());
                        Log.i("TAG", "onResponse: isActive --> "+obj.getString("error"));
                        listener.onFailure(obj.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }else {
                    listener.onResponse(response.body());
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                listener.onFailure(t.getMessage());
            }
        });
    }
}
