package com.kotaksarapan.kotaksarapan.rest.sender;

import android.util.Log;

import com.kotaksarapan.kotaksarapan.model.Login.RegisterModel;
import com.kotaksarapan.kotaksarapan.model.Login.RegisterResponse;
import com.kotaksarapan.kotaksarapan.rest.ASRetrofitRequest;
import com.kotaksarapan.kotaksarapan.rest.ASRetrofitRequestBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by wresniwahyu on 19/07/2017.
 */

public class RegisterSender extends ASRetrofitRequestBuilder{
    public interface RegisterListener{
        void onResponse(RegisterResponse response);
        void onFailure(String m);
    }

    public void send(String token, RegisterModel model, final RegisterListener listener){
        ASRetrofitRequest request = ASRetrofitRequestBuilder.createService(ASRetrofitRequest.class, token);
        Call<RegisterResponse> callback = request.sendRegister(model);
        callback.enqueue(new Callback<RegisterResponse>() {
            @Override
            public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                if(response.body() == null){
                    try {
                        JSONObject obj = new JSONObject(response.errorBody().string());
                        Log.i("TAG", "onResponse: "+obj.toString());
                        listener.onFailure(obj.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }else {
                    listener.onResponse(response.body());
                }
            }

            @Override
            public void onFailure(Call<RegisterResponse> call, Throwable t) {
                listener.onFailure(t.getMessage());
            }
        });
    }

}
