package com.kotaksarapan.kotaksarapan.rest.loader;

import com.kotaksarapan.kotaksarapan.model.side.SideResponse;
import com.kotaksarapan.kotaksarapan.rest.ASRetrofitRequest;
import com.kotaksarapan.kotaksarapan.rest.ASRetrofitRequestBuilder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by wresniwahyu on 14/09/2017.
 */

public class SideLoader extends ASRetrofitRequestBuilder{
    public interface SideListener{
        void onComplete(SideResponse response);
        void onError(String m);
    }

    public void load(String token, final SideListener listener){
        ASRetrofitRequest request = ASRetrofitRequestBuilder.createService(ASRetrofitRequest.class, token);
        Call<SideResponse> count = request.getSides();
        count.enqueue(new Callback<SideResponse>() {
            @Override
            public void onResponse(Call<SideResponse> call, Response<SideResponse> response) {
                listener.onComplete(response.body());
            }

            @Override
            public void onFailure(Call<SideResponse> call, Throwable t) {
                listener.onError(t.getMessage());
            }
        });
    }
}
