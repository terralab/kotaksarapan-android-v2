package com.kotaksarapan.kotaksarapan.rest.sender;

import com.kotaksarapan.kotaksarapan.model.Login.LoginResponse;

/**
 * Created by wresniwahyu on 20/07/2017.
 */

public interface LoginListener {
    void onResponse(LoginResponse response);
    void onFailure(String m);
}
