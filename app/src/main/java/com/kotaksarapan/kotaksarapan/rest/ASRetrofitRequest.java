package com.kotaksarapan.kotaksarapan.rest;

import com.kotaksarapan.kotaksarapan.model.Category.CategoryResponse;
import com.kotaksarapan.kotaksarapan.model.Cook.CookResponse;
import com.kotaksarapan.kotaksarapan.model.ListMeals.MealsResponse;
import com.kotaksarapan.kotaksarapan.model.Login.LoginModel;
import com.kotaksarapan.kotaksarapan.model.Login.LoginProviderModel;
import com.kotaksarapan.kotaksarapan.model.Login.LoginResponse;
import com.kotaksarapan.kotaksarapan.model.Meal.MealResponse;
import com.kotaksarapan.kotaksarapan.model.Login.RegisterModel;
import com.kotaksarapan.kotaksarapan.model.Login.RegisterResponse;
import com.kotaksarapan.kotaksarapan.model.Token.TokenStatusResponse;
import com.kotaksarapan.kotaksarapan.model.address.AddressModel;
import com.kotaksarapan.kotaksarapan.model.address.AddressResponse;
import com.kotaksarapan.kotaksarapan.model.address.AddressesResponse;
import com.kotaksarapan.kotaksarapan.model.balance.BalanceResponse;
import com.kotaksarapan.kotaksarapan.model.order.CreateOrderModel;
import com.kotaksarapan.kotaksarapan.model.order.CreateOrderResponse;
import com.kotaksarapan.kotaksarapan.model.order.ListOrderModel;
import com.kotaksarapan.kotaksarapan.model.order.OrderModel;
import com.kotaksarapan.kotaksarapan.model.order.OrderResponse;
import com.kotaksarapan.kotaksarapan.model.orderitem.OrderItemResponse;
import com.kotaksarapan.kotaksarapan.model.side.SideResponse;
import com.kotaksarapan.kotaksarapan.model.topup.TopUpCreditModel;
import com.kotaksarapan.kotaksarapan.model.topup.TopUpResponse;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by wresniwahyu on 5/8/2017.
 */

public interface ASRetrofitRequest{

    @POST("address/create")
    Call<AddressResponse> sendAddress(@Body AddressModel address);

    @GET("category/index")
    Call<CategoryResponse> getCategory();

    @GET("meal/category/{id}")
    Call<MealsResponse> getMeals(@Path("id") int id,
                                 @Query("page") int page);

    @GET("meals/favorite") //ini belum di server
    Call<MealsResponse> getFavorite();

    @GET("meal/{id}")
    Call<MealResponse> getMeal(@Path("id") int id);

    @GET("meal/cook/{id}")
    Call<MealsResponse> getCookMeals(@Path("id") int id);

    @GET("meal/search")
    Call<MealsResponse> getSearchByTitle(@Query("title") String title);

    @GET("cook/{id}")
    Call<CookResponse> getCook(@Path("id") int id);

    @GET("user/whois")
    Call<TokenStatusResponse> checkToken();

    @GET("user/address")
    Call<AddressesResponse> getAddresses();

    @GET("user/balance")
    Call<BalanceResponse> getBalance();

    @POST("user/login/public")
    Call<LoginResponse> sendPublicLogin(@Body LoginModel login);

    @POST("user/register")
    Call<RegisterResponse> sendRegister(@Body RegisterModel register);

    @POST("user/login/provider")
    Call<LoginResponse> sendLoginProvider(@Body LoginProviderModel model);

    @POST("user/login")
    Call<LoginResponse> sendLogin(@Body LoginModel login);

    @POST("payment/topup")
    Call<TopUpResponse> sendTopUp(@Body TopUpCreditModel topup);

    @POST("order/create")
    Flowable<CreateOrderResponse> createOrder(@Body RequestBody body);

    @GET("side") //ini belum di server
    Call<SideResponse> getSides();

    @POST("order") //ini belum di server
    Call<OrderResponse> sendOrder(@Body OrderModel order);

    @POST("listorder") //ini belum di server
    Call<OrderItemResponse> sendListOrder(@Body ArrayList<ListOrderModel> item);
}
