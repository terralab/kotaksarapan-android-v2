package com.kotaksarapan.kotaksarapan.rest.sender;

import com.kotaksarapan.kotaksarapan.interfaces.TopUpListener;
import com.kotaksarapan.kotaksarapan.model.topup.TopUpCreditModel;
import com.kotaksarapan.kotaksarapan.model.topup.TopUpResponse;
import com.kotaksarapan.kotaksarapan.rest.ASRetrofitRequest;
import com.kotaksarapan.kotaksarapan.rest.ASRetrofitRequestBuilder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by wresniwahyu on 18/09/2017.
 */

public class TopUpSender extends ASRetrofitRequestBuilder{
    public void send(String token, TopUpCreditModel model, final TopUpListener listener){
        ASRetrofitRequest request = ASRetrofitRequestBuilder.createService(ASRetrofitRequest.class,token);
        Call<TopUpResponse> callback = request.sendTopUp(model);
        callback.enqueue(new Callback<TopUpResponse>() {
            @Override
            public void onResponse(Call<TopUpResponse> call, Response<TopUpResponse> response) {
                listener.onResponse(response.body());
            }

            @Override
            public void onFailure(Call<TopUpResponse> call, Throwable t) {
                listener.onFailure(t.getMessage());
            }
        });
    }
}
