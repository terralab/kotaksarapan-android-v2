package com.kotaksarapan.kotaksarapan.rest.sender;

import com.kotaksarapan.kotaksarapan.model.order.OrderModel;
import com.kotaksarapan.kotaksarapan.model.order.OrderResponse;
import com.kotaksarapan.kotaksarapan.rest.ASRetrofitRequest;
import com.kotaksarapan.kotaksarapan.rest.ASRetrofitRequestBuilder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by wresniwahyu on 24/08/2017.
 */

public class OrderSender extends ASRetrofitRequestBuilder {
    public interface OrderListener{
        void onResponse(OrderResponse response);
        void onFailure(String m);
    }

    public void send(String token, OrderModel model, final OrderListener listener){
        ASRetrofitRequest request = ASRetrofitRequestBuilder.createService(ASRetrofitRequest.class, token);
        Call<OrderResponse> callback = request.sendOrder(model);
        callback.enqueue(new Callback<OrderResponse>() {
            @Override
            public void onResponse(Call<OrderResponse> call, Response<OrderResponse> response) {
                listener.onResponse(response.body());
            }

            @Override
            public void onFailure(Call<OrderResponse> call, Throwable t) {
                listener.onFailure(t.getMessage());
            }
        });
    }
}
