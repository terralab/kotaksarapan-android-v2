package com.kotaksarapan.kotaksarapan.rest.sender;

import android.util.Log;

import com.kotaksarapan.kotaksarapan.model.order.ListOrderModel;
import com.kotaksarapan.kotaksarapan.model.orderitem.OrderItemResponse;
import com.kotaksarapan.kotaksarapan.model.sqlite.OrderItemModel;
import com.kotaksarapan.kotaksarapan.rest.ASRetrofitRequest;
import com.kotaksarapan.kotaksarapan.rest.ASRetrofitRequestBuilder;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by wresniwahyu on 28/08/2017.
 */

public class ListOrderSender extends ASRetrofitRequestBuilder{
    public interface ListOrderListener{
        void onResponse(OrderItemResponse response);
        void onFailure(String m);
    }

    public void send(String token, ArrayList<ListOrderModel> model, final ListOrderListener listener){
        Log.i("TAG", "send: onresponse model"+model);
        ASRetrofitRequest request = ASRetrofitRequestBuilder.createService(ASRetrofitRequest.class,token);
        Call<OrderItemResponse> callback = request.sendListOrder(model);
        callback.enqueue(new Callback<OrderItemResponse>() {
            @Override
            public void onResponse(Call<OrderItemResponse> call, Response<OrderItemResponse> response) {
                listener.onResponse(response.body());
            }

            @Override
            public void onFailure(Call<OrderItemResponse> call, Throwable t) {
                listener.onFailure(t.getMessage());
            }
        });
    }
}
