package com.kotaksarapan.kotaksarapan.rest.loader;

import com.kotaksarapan.kotaksarapan.model.Category.CategoryResponse;
import com.kotaksarapan.kotaksarapan.rest.ASRetrofitRequest;
import com.kotaksarapan.kotaksarapan.rest.ASRetrofitRequestBuilder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by wresniwahyu on 5/24/2017.
 */

public class CategoryLoader extends ASRetrofitRequestBuilder{
    public interface CategoryListener{
        void onComplete(CategoryResponse data);
        void onError(String m);
    }

    public void load(String token, final CategoryListener listener){
        ASRetrofitRequest request = ASRetrofitRequestBuilder.createService(ASRetrofitRequest.class, token);
        Call<CategoryResponse> count = request.getCategory();
        count.enqueue(new Callback<CategoryResponse>() {
            @Override
            public void onResponse(Call<CategoryResponse> call, Response<CategoryResponse> response) {
                listener.onComplete(response.body());
            }

            @Override
            public void onFailure(Call<CategoryResponse> call, Throwable t) {
                listener.onError(t.getMessage());
            }
        });
    }
}
