package com.kotaksarapan.kotaksarapan.rest.loader;

import com.kotaksarapan.kotaksarapan.model.Category.CategoryResponse;
import com.kotaksarapan.kotaksarapan.model.ListMeals.MealsResponse;
import com.kotaksarapan.kotaksarapan.model.Meal.MealResponse;
import com.kotaksarapan.kotaksarapan.rest.ASRetrofitRequest;
import com.kotaksarapan.kotaksarapan.rest.ASRetrofitRequestBuilder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by wresniwahyu on 04/07/2017.
 */

public class FavoriteLoader extends ASRetrofitRequestBuilder {
    public void load (String token, final MealsLoader.MealsListener listener){
        ASRetrofitRequest request = ASRetrofitRequestBuilder.createService(ASRetrofitRequest.class, token);
        Call<MealsResponse> count = request.getFavorite();
        count.enqueue(new Callback<MealsResponse>(){
            @Override
            public void onResponse(Call<MealsResponse> call, Response<MealsResponse> response) {
                listener.onComplete(response.body());
            }

            @Override
            public void onFailure(Call<MealsResponse> call, Throwable t) {
                listener.onError(t.getMessage());
            }
        });
    }
}
