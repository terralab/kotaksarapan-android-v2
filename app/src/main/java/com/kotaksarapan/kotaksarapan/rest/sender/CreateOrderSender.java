package com.kotaksarapan.kotaksarapan.rest.sender;

import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.kotaksarapan.kotaksarapan.interfaces.CreateOrderListener;
import com.kotaksarapan.kotaksarapan.model.order.CreateOrderModel;
import com.kotaksarapan.kotaksarapan.model.order.CreateOrderResponse;
import com.kotaksarapan.kotaksarapan.rest.ASRetrofitRequest;
import com.kotaksarapan.kotaksarapan.rest.ASRetrofitRequestBuilder;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MultipartBody;
import retrofit2.Callback;

/**
 * Created by wresniwahyu on 25/09/2017.
 */

public class CreateOrderSender extends ASRetrofitRequestBuilder{
    private CompositeDisposable disposable = new CompositeDisposable();

    public void send(String token, MultipartBody body, final CreateOrderListener listener){
        ASRetrofitRequest request = ASRetrofitRequestBuilder.createService(ASRetrofitRequest.class,token);
        Disposable subscribe = request.createOrder(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<CreateOrderResponse>() {
                    @Override
                    public void accept(CreateOrderResponse response) throws Exception {
                        listener.onResponse(response);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Log.e("ERROR_TAG", "accept: ", throwable);

                        listener.onFailure("Failed");
                    }
                });
        disposable.add(subscribe);
    }

}
