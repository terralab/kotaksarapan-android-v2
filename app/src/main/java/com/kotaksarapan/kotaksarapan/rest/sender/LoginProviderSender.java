package com.kotaksarapan.kotaksarapan.rest.sender;

import android.util.Log;

import com.kotaksarapan.kotaksarapan.model.Login.LoginProviderModel;
import com.kotaksarapan.kotaksarapan.model.Login.LoginResponse;
import com.kotaksarapan.kotaksarapan.rest.ASRetrofitRequest;
import com.kotaksarapan.kotaksarapan.rest.ASRetrofitRequestBuilder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by wresniwahyu on 15/08/2017.
 */

public class LoginProviderSender extends ASRetrofitRequestBuilder {
    public void send(String token, LoginProviderModel model, final LoginListener listener){
        ASRetrofitRequest request = ASRetrofitRequestBuilder.createService(ASRetrofitRequest.class, token);
        Call<LoginResponse> callback = request.sendLoginProvider(model);
        callback.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                listener.onResponse(response.body());
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                listener.onFailure(t.getMessage());
            }
        });
    }
}
