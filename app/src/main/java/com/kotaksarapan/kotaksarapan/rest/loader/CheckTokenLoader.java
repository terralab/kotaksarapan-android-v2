package com.kotaksarapan.kotaksarapan.rest.loader;

import android.util.Log;

import com.kotaksarapan.kotaksarapan.model.Token.TokenStatusResponse;
import com.kotaksarapan.kotaksarapan.rest.ASRetrofitRequest;
import com.kotaksarapan.kotaksarapan.rest.ASRetrofitRequestBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by wresniwahyu on 08/08/2017.
 */

public class CheckTokenLoader extends ASRetrofitRequestBuilder {
    public interface CheckTokenListener{
        void onComplete(TokenStatusResponse data);
        void onError(String m);
    }

    public void load(String token, final CheckTokenListener listener){
        final ASRetrofitRequest request =  ASRetrofitRequestBuilder.createService(ASRetrofitRequest.class, token);
        Call<TokenStatusResponse> count = request.checkToken();
        count.enqueue(new Callback<TokenStatusResponse>() {
            @Override
            public void onResponse(Call<TokenStatusResponse> call, Response<TokenStatusResponse> response) {

                if(response.body() == null){
                    try {
                        JSONObject obj = new JSONObject(response.errorBody().string());
                        Log.i("TAG", "onResponse: "+obj.toString());
                        Log.i("TAG", "onResponse: isActive --> "+obj.getString("isActive"));
                        listener.onError(obj.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }else {
                    listener.onComplete(response.body());
                }
            }

            @Override
            public void onFailure(Call<TokenStatusResponse> call, Throwable t) {
                listener.onError(t.getMessage());
            }
        });
    }
}
