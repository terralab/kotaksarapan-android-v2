package com.kotaksarapan.kotaksarapan.interfaces;

import com.kotaksarapan.kotaksarapan.model.address.AddressResponse;

/**
 * Created by wresniwahyu on 19/09/2017.
 */

public interface AddressListener {
    void onResponse(AddressResponse response);
    void onFailure(String m);
}
