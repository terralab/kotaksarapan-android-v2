package com.kotaksarapan.kotaksarapan.interfaces;

import com.kotaksarapan.kotaksarapan.model.Meal.MealModel;

/**
 * Created by wresniwahyu on 08/08/2017.
 */

public interface OnCartClick {
    void onClickCart(String cookID, String mealID, String mealName, int price, int originalPrice, int amount);
}
