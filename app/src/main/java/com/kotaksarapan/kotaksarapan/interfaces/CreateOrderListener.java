package com.kotaksarapan.kotaksarapan.interfaces;

import com.kotaksarapan.kotaksarapan.model.order.CreateOrderResponse;

import java.util.List;

import retrofit2.Callback;
import retrofit2.http.POST;

/**
 * Created by wresniwahyu on 25/09/2017.
 */

public interface CreateOrderListener {
    void onResponse(CreateOrderResponse response);
    void onFailure(String m);
}
