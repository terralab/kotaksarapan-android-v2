package com.kotaksarapan.kotaksarapan.interfaces;

import com.kotaksarapan.kotaksarapan.model.topup.TopUpResponse;

/**
 * Created by wresniwahyu on 18/09/2017.
 */

public interface TopUpListener {
    void onResponse(TopUpResponse response);
    void onFailure(String m);
}
