package com.kotaksarapan.kotaksarapan.interfaces;

import com.kotaksarapan.kotaksarapan.model.address.AddressesResponse;

/**
 * Created by wresniwahyu on 20/09/2017.
 */

public interface AddressesListener {
    void onResponse(AddressesResponse response);
    void onError(String m);
}
