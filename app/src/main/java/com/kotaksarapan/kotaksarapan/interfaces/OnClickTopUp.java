package com.kotaksarapan.kotaksarapan.interfaces;

/**
 * Created by wresniwahyu on 18/09/2017.
 */

public interface OnClickTopUp {
    void onClickCreditAmount(float amount);
}
