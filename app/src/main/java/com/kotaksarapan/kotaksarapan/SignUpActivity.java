package com.kotaksarapan.kotaksarapan;

import android.app.DialogFragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.kotaksarapan.kotaksarapan.Helper.AppHelper;
import com.kotaksarapan.kotaksarapan.Helper.SessionManager;
import com.kotaksarapan.kotaksarapan.model.Login.LoginProviderModel;
import com.kotaksarapan.kotaksarapan.model.Login.LoginResponse;
import com.kotaksarapan.kotaksarapan.model.Login.RegisterModel;
import com.kotaksarapan.kotaksarapan.model.Login.RegisterResponse;
import com.kotaksarapan.kotaksarapan.rest.sender.LoginListener;
import com.kotaksarapan.kotaksarapan.rest.sender.LoginProviderSender;
import com.kotaksarapan.kotaksarapan.rest.sender.RegisterSender;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private Button btnGoogle, btnFacebook;
    private EditText txtFullName, txtEmail, txtPhoneNumber, txtPassword;
    private TextInputLayout inputEmail, inputPhoneNumber;
    private FloatingActionButton fabNext;
    private TextView tvForgot;

    private SessionManager session;
    private GoogleApiClient mGoogleApiClient;
    private CallbackManager callbackManager;
    private String socialName, socialEmail, facebookID, googleID = null;

    private String id;
    private String name;
    private String email;
    private String picture;
    private String token;

    private LoginProviderSender providerSender;
    private LoginProviderModel providerModel;

    private RegisterSender sender;
    private RegisterModel register;

    private Boolean validEmail, validPhone = false;

    private String usernameInput, userPassword;

    private FragmentTransaction ft ;
    private DialogFragment loading ;

    private com.kotaksarapan.kotaksarapan.Helper.AppHelper mApplication;
    private SharedPreferences mPrefs;

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        session = new SessionManager(getApplicationContext());

        mGoogleApiClient = AppHelper.getGoogleApiHelper().getGoogleApiClient();

        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mApplication = (AppHelper) getApplication();
        mApplication.setContext(this);
        mPrefs = this.getSharedPreferences("COOKIES", MODE_PRIVATE);

        btnGoogle = (Button) findViewById(R.id.btnGoogle);
        btnFacebook = (Button) findViewById(R.id.btnFacebook);
        txtFullName = (EditText) findViewById(R.id.txtFullName);
        txtEmail = (EditText) findViewById(R.id.txtEmail);
        txtPhoneNumber = (EditText) findViewById(R.id.txtPhoneNumber);
        txtPassword = (EditText) findViewById(R.id.txtPassword);
        inputEmail = (TextInputLayout) findViewById(R.id.inputEmail);
        inputPhoneNumber = (TextInputLayout) findViewById(R.id.inputPhoneNumber);
        fabNext = (FloatingActionButton) findViewById(R.id.fabNext);
        tvForgot = (TextView) findViewById(R.id.tvForgot);

        txtFullName.addTextChangedListener(new SignUpActivity.MyTextWatcher(txtFullName));
        txtEmail.addTextChangedListener(new SignUpActivity.MyTextWatcher(txtEmail));
        txtPhoneNumber.addTextChangedListener(new SignUpActivity.MyTextWatcher(txtPhoneNumber));
        txtPassword.addTextChangedListener(new SignUpActivity.MyTextWatcher(txtPassword));

        btnGoogle.setOnClickListener(this);
        btnFacebook.setOnClickListener(this);
        fabNext.setOnClickListener(this);
        tvForgot.setOnClickListener(this);
    }

    private void doProviderLogin(String provider){
        providerModel = new LoginProviderModel();
        providerModel.setEmail(email);
        providerModel.setName(name);
        providerModel.setProvider(provider);
        providerModel.setProvider_id(id);

        providerSender = new LoginProviderSender();
        providerSender.send(session.getAuthToken(), providerModel, new LoginListener() {
            @Override
            public void onResponse(LoginResponse response) {
                Log.i("facebook", "onResponse: login provider --> "+response.isStatus());
                Log.i("facebook", "onResponse: login provider --> "+response.getMessage());
                Log.i("facebook", "onResponse: login provider --> "+response.getToken());

                int userID = response.getUser().getId();

                session.setLogin(userID, name, email, picture, token);
                startActivity(new Intent(SignUpActivity.this, MealsDrawerActivity.class));
                finish();
            }

            @Override
            public void onFailure(String m) {

            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 0) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    //sign in via Google
    private void doSignInGoogle(){
        Intent googleSignIn = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(googleSignIn, 0);
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d("TAG", "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.

            GoogleSignInAccount acct = result.getSignInAccount();
            token = acct.getIdToken();
            name = acct.getDisplayName();
            email = acct.getEmail();
            picture = acct.getPhotoUrl().toString();
            id = acct.getId();

            Toast.makeText(this,"Hello "+name,Toast.LENGTH_SHORT).show();
            doProviderLogin("google");
        } else {
            // Signed out, show unauthenticated UI.
            Log.i("TAG", "handleSignInResult: token is null");
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
    //end of sign in via Google

    //sign in via facebook
    private void doSignInFacebook(){
        Log.i("TAG", "onSuccess: login proses");
        LoginManager.getInstance().logInWithReadPermissions(SignUpActivity.this, Arrays.asList("email"));
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                new GetFbUserInfo(loginResult).execute();
                Log.i("TAG", "onSuccess: login success");
//                setFacebookSession(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Log.i("TAG", "onSuccess: login cancel");
                Toast.makeText(SignUpActivity.this, "Facebook login cancelled",
                        Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(FacebookException error) {
                Log.i("TAG", "onSuccess: login error --> "+error);
                Toast.makeText(SignUpActivity.this, "Error in Facebook login " +
                        error.getMessage(), Toast.LENGTH_LONG).show();
//                        LoginManager.getInstance().logOut();
            }
        });
    }

    private class GetFbUserInfo extends AsyncTask<Void, Void, String> {
        private final LoginResult loginResult;
//        private ProgressDialog dialog;

        public GetFbUserInfo(LoginResult loginResult) {
            this.loginResult = loginResult;
        }

        @Override
        protected void onPreExecute() {
//            dialog = ProgressDialog.show(LoginActivity.this, "Wait", "Getting user name");
            showDialog();
        }

        @Override
        protected String doInBackground(Void... params) {
            GraphRequest request = GraphRequest.newMeRequest(
                    loginResult.getAccessToken(),
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(JSONObject object, GraphResponse response) {
                            // Application code
                            Log.v("LoginActivity", response.toString());
                        }
                    });
            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,name,email,picture{url}");
            request.setParameters(parameters);
            GraphResponse graphResponse = request.executeAndWait();
            try {
                id = graphResponse.getJSONObject().getString("id");
                name = graphResponse.getJSONObject().getString("name");
                email = graphResponse.getJSONObject().getString("email");
                picture = graphResponse.getJSONObject().getJSONObject("picture").getJSONObject("data").getString("url");
                token = loginResult.getAccessToken().getToken();

                return name;
            } catch (JSONException e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(String response) {
//            dialog.dismiss();
            txtEmail.setEnabled(false);
            loading.dismiss();
            if (response != null) {
                Toast.makeText(SignUpActivity.this, "Hello " + response, Toast.LENGTH_SHORT).show();
                doProviderLogin("facebook");
            } else {
                Toast.makeText(SignUpActivity.this, "Unable to get user name from Facebook",
                        Toast.LENGTH_LONG).show();
            }
        }
    }
    //end of sign in via facebook

    private void doRegister(){
        final String email = txtEmail.getText().toString();
        final String password = txtPassword.getText().toString();
        final String name = txtFullName.getText().toString();
        final String phone = txtPhoneNumber.getText().toString();

        register = new RegisterModel();
        register.setEmail(email);
        register.setPassword(password);
        register.setName(name);
        register.setPhone(phone);

        sender = new RegisterSender();
        sender.send(session.getAuthToken(),register, new RegisterSender.RegisterListener() {
            @Override
            public void onResponse(RegisterResponse response) {
                Log.i("TAG", "onResponse: register  status  --> "+response.isStatus());
                Log.i("TAG", "onResponse: register  message  --> "+response.getMessage());
                Log.i("TAG", "onResponse: register  token  --> "+response.getToken());

                int userID = response.getUser().getId();
                String token = response.getToken();

                session.setLogin(userID, name, email, "", token);
                Toast.makeText(getApplicationContext(), response.getMessage(), Toast.LENGTH_LONG).show();
                startActivity(new Intent(SignUpActivity.this, MealsDrawerActivity.class));
            }

            @Override
            public void onFailure(String m) {
                Log.i("TAG", "onFailure: "+m);
                try {
                    JSONObject obj = new JSONObject(m);
                    String isActive = obj.getString("isActive");
                    Log.i("TAG", "onFailure: register --> "+isActive);
                    if(isActive == "false"){

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    void showDialog() {
        ft = getFragmentManager().beginTransaction();
        loading = LoadingActivity.newInstance();
        loading.show(ft, "dialog");
    }

    public void setActivityBackgroundColor(int color) {
        View view = this.getWindow().getDecorView();
        view.setBackgroundColor(color);
    }

    private boolean validateEmail() {
        String email = txtEmail.getText().toString().trim();

        if (email.isEmpty() || !isValidEmail(email)) {
            validEmail = false;
            inputEmail.setError("email invalid!");
            requestFocus(txtEmail);
            return false;
        } else {
            inputEmail.setErrorEnabled(false);
            validEmail = true;
        }

        return true;
    }

    private boolean validatePhoneNumber() {
        String phone = txtPhoneNumber.getText().toString().trim();

        if (phone.isEmpty() || !isValidPhoneNumber(phone)) {
            validPhone = false;
            inputPhoneNumber.setError("phone number invalid!");
            requestFocus(txtPhoneNumber);
            return false;
        } else {
            inputPhoneNumber.setErrorEnabled(false);
            validPhone = true;
        }

        return true;
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private static boolean isValidPhoneNumber(String phone) {
        return !TextUtils.isEmpty(phone) && android.util.Patterns.PHONE.matcher(phone).matches();
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void validateComplete(){
        if (!txtFullName.getText().toString().isEmpty() && !txtEmail.getText().toString().isEmpty() && !txtPhoneNumber.getText().toString().isEmpty() && !txtPassword.getText().toString().isEmpty() && validEmail && validPhone){
            setActivityBackgroundColor(0xfffec713);
            fabNext.setVisibility(View.VISIBLE);
        }else{
            setActivityBackgroundColor(0xfffbfbfb);
            fabNext.setVisibility(View.GONE);
        }
    }

    private class MyTextWatcher implements TextWatcher {
        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.txtFullName:
                    validateComplete();
                    break;
                case R.id.txtEmail:
                    validateEmail();
                    validateComplete();
                    break;
                case R.id.txtPhoneNumber:
                    validatePhoneNumber();
                    validateComplete();
                    break;
                case R.id.txtPassword:
                    validateComplete();
                    break;
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnGoogle:
                doSignInGoogle();
//                Toast.makeText(this,"Login with G+",Toast.LENGTH_SHORT).show();
                break;
            case R.id.btnFacebook:
//                Toast.makeText(this,"Login with FB",Toast.LENGTH_SHORT).show();
                doSignInFacebook();
                break;
            case R.id.fabNext:
                doRegister();
//                signUp();
                break;
            case R.id.tvForgot:
                Toast.makeText(this,"Forgot Password Activity",Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
