package com.kotaksarapan.kotaksarapan;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kotaksarapan.kotaksarapan.Helper.SessionManager;
import com.kotaksarapan.kotaksarapan.adapter.CartAdapter;
import com.kotaksarapan.kotaksarapan.interfaces.OnClickAmount;
import com.kotaksarapan.kotaksarapan.model.cart.CartModel;
import com.kotaksarapan.kotaksarapan.Helper.MySQLHelper;
import com.kotaksarapan.kotaksarapan.model.sqlite.OrderItemModel;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class CartActivity extends AppCompatActivity implements OnClickAmount, NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {
    private LinearLayoutManager manager;
    private RecyclerView rvCart;
    private CartAdapter adapter;
    private List<CartModel> cartList;

    private MySQLHelper db;
    private SessionManager session;

    private Button btnPay;
    private LinearLayout delivCostLayout, detailCostLayout;
    private ImageView imgExpand;

    private int total = 0;
    private TextView txtTotalPrice;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        db = MySQLHelper.getInstance(getBaseContext());
        session = new SessionManager(getApplicationContext());

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.setDrawerIndicatorEnabled(true);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        btnPay = (Button) findViewById(R.id.btnPay);
        delivCostLayout = (LinearLayout) findViewById(R.id.delivCostLayout);
        detailCostLayout = (LinearLayout) findViewById(R.id.detailCostLayout);
        imgExpand = (ImageView) findViewById(R.id.imgExpand);
        txtTotalPrice = (TextView) findViewById(R.id.txtTotalPrice);

        btnPay.setOnClickListener(this);
        delivCostLayout.setOnClickListener(this);
        detailCostLayout.setOnClickListener(this);

        cartList = new ArrayList<>();
        adapter = new CartAdapter(cartList, txtTotalPrice, getApplicationContext(), this);

        rvCart = (RecyclerView) findViewById(R.id.rvCart);
        manager = new LinearLayoutManager(getApplicationContext());
        rvCart.setLayoutManager(manager);
        rvCart.setAdapter(adapter);

        for(OrderItemModel data : db.getAllOrderItem())
        {
            CartModel cart = new CartModel();
            cart.setMealid(data.getMealid());
            cart.setMeal(data.getMealname());
            cart.setPrice(Integer.parseInt(data.getPrice()));
            cart.setAmount(Integer.parseInt(data.getAmount()));
            total = total + (Integer.parseInt(data.getPrice()) * Integer.parseInt(data.getAmount()));
            cartList.add(cart);
            adapter.notifyDataSetChanged();
        }

        txtTotalPrice.setText(total+"");
    }

    private void saveTotal(){
        final int total = Integer.parseInt(txtTotalPrice.getText().toString());
        if(db.isEmptyOrderInfo()){
            db.addTotal(session.getID(), total);
            db.close();
        }else{
            db.updateTotal(session.getID(), total);
            db.close();
        }
    }

    private void getTotal(){
        total = 0;
        for(OrderItemModel data : db.getAllOrderItem())
        {
            Log.i("TAG", "onClickItemAmount: "+data.getAmount()+ " "+data.getPrice());
            total = total + (Integer.parseInt(data.getPrice()) * Integer.parseInt(data.getAmount()));
            Log.i("TAG", "total onClickItemAmount: "+total);
        }
        txtTotalPrice.setText(total+"");
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.search) {
            //todo add action here
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            startActivity(new Intent(CartActivity.this,MealsDrawerActivity.class));
        } else if (id == R.id.nav_dashboard) {
            if(session.isLoggedIn()){
                startActivity(new Intent(CartActivity.this, DashboardActivity.class));
            }else{
                startActivity(new Intent(CartActivity.this,MenuLoginActivity.class));
            }
        } else if (id == R.id.nav_favourite) {
            startActivity(new Intent(CartActivity.this,FavoritesActivity.class));
        } else if (id == R.id.nav_promotion) {
            startActivity(new Intent(CartActivity.this,PromotionsActivity.class));
        } else if (id == R.id.nav_about) {
            startActivity(new Intent(CartActivity.this,AboutUsActivity.class));
        } else if (id == R.id.nav_homecook) {
            startActivity(new Intent(CartActivity.this,BeHomecookActivity.class));
        } else if (id == R.id.nav_setting) {
            startActivity(new Intent(CartActivity.this,SettingActivity.class));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnPay:
                if(session.isLoggedIn()) {
                    saveTotal();
                    startActivity(new Intent(CartActivity.this, DeliveryAddressActivity.class));
                }else {
                    startActivity(new Intent(CartActivity.this, MenuLoginActivity.class));
                    Toast.makeText(getApplicationContext(),"you must login first",Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.delivCostLayout:
                if (detailCostLayout.getVisibility() == View.GONE){
                    imgExpand.setImageResource(R.drawable.ic_expand_more_black_24dp);
                    detailCostLayout.setVisibility(View.VISIBLE);
                }else {
                    imgExpand.setImageResource(R.drawable.ic_expand_less_black_24dp);
                    detailCostLayout.setVisibility(View.GONE);
                }
                break;
        }
    }

    @Override
    public void onClickItemAmount() {
        getTotal();
    }
}
