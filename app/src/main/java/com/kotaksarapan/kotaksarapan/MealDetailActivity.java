package com.kotaksarapan.kotaksarapan;

import android.app.DialogFragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.kotaksarapan.kotaksarapan.Helper.MySQLHelper;
import com.kotaksarapan.kotaksarapan.Helper.SessionManager;
import com.kotaksarapan.kotaksarapan.model.Meal.MealResponse;
import com.kotaksarapan.kotaksarapan.rest.loader.MealLoader;

import java.text.NumberFormat;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MealDetailActivity extends AppCompatActivity implements View.OnClickListener{
    private TextView txtMeal, txtPrice, txtHomecookName, txtMealDescription;
    private RatingBar rtbMealRating;
    private ImageView imgMeal;
    private CircleImageView imgHomecook;
    private ImageButton btnCart, btnRate;
    private FloatingActionButton fabCart;

    private SessionManager session;
    private MySQLHelper db;
    private FragmentTransaction ft ;
    private DialogFragment loading ;

    private MealLoader mealLoader;

    private int mealID;
    private int cookID;
    private String mealName;
    private int price;
    private int originalPrice = 0;
    private int amount = 1;

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meal_detail);

        session = new SessionManager(getApplicationContext());
        db = MySQLHelper.getInstance(getApplicationContext());

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        txtMeal = (TextView) findViewById(R.id.txtMeal);
        txtPrice = (TextView) findViewById(R.id.txtPrice);
        txtHomecookName = (TextView) findViewById(R.id.txtHomecookName);
        txtMealDescription = (TextView) findViewById(R.id.txtMealDescription);
        rtbMealRating = (RatingBar) findViewById(R.id.rtbMealRating);
        imgMeal = (ImageView) findViewById(R.id.imgMeal);
        imgHomecook = (CircleImageView) findViewById(R.id.imgHomecook);
        btnCart = (ImageButton) findViewById(R.id.btnCart);
        btnRate = (ImageButton) findViewById(R.id.btnRate);
        fabCart = (FloatingActionButton) findViewById(R.id.fabCart);

        btnCart.setOnClickListener(this);
        fabCart.setOnClickListener(this);

        showDialog();

        Intent intent = getIntent();
        mealID = intent.getIntExtra("id",0);
        getMealDetail(mealID);

        checkCart();

    }

    private void getMealDetail(int id){
        mealLoader = new MealLoader();
        mealLoader.load(session.getAuthToken(),id, new MealLoader.MealListener() {
            @Override
            public void onComplete(MealResponse data) {
                Log.i("TAG", "onComplete: get meal detail");
                loading.dismiss();

                Locale localeID = new Locale("in","ID");
                NumberFormat rupiah = NumberFormat.getInstance(localeID);
                double mealprice = data.getData().getAttributes().getPrice();
                String priceRp = rupiah.format(mealprice);

                cookID = data.getData().getAttributes().get_cookid();
                mealName = data.getData().getAttributes().getTitle();
                price = (int) mealprice;

                txtMeal.setText(data.getData().getAttributes().getTitle());
                txtPrice.setText("Rp. "+priceRp+",-");
                txtMealDescription.setText(Html.fromHtml(data.getData().getAttributes().getDescription()));
                rtbMealRating.setRating(data.getData().getAttributes().getRating()/2);
                txtHomecookName.setText(data.getData().getAttributes().getCook().getNickname());
                Glide.with(getApplicationContext()).load(data.getData().getAttributes().getImagesmall()).placeholder(R.drawable.placeholder_food).dontAnimate().into(imgMeal);
                Glide.with(getApplicationContext()).load(data.getData().getAttributes().getCook().getImagecook()).placeholder(R.drawable.placeholder_user).dontAnimate().into(imgHomecook);
            }

            @Override
            public void onError(String m) {
                Log.i("TAG", "onError: get meal detail error --> "+m);
            }
        });
    }

    void showDialog() {
        ft = getFragmentManager().beginTransaction();
        loading = LoadingActivity.newInstance();
        loading.show(ft, "dialog");
    }

    public void checkCart(){
        if(db.isEmptyItem()){
            fabCart.setVisibility(View.GONE);
        }else {
            fabCart.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnCart:
                if(db.isItemExist(String.valueOf(mealID))){
                    Toast.makeText(getApplicationContext(),"already in cart",Toast.LENGTH_SHORT).show();
                }else {
                    db.addItem(String.valueOf(cookID), String.valueOf(mealID), mealName, price, originalPrice, amount);
                    checkCart();
                }
                break;
            case R.id.fabCart:
                startActivity(new Intent(MealDetailActivity.this,CartActivity.class));
                break;
        }
    }
}
