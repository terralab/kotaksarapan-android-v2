package com.kotaksarapan.kotaksarapan;

import android.app.DialogFragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class LanguageActivity extends AppCompatActivity implements View.OnClickListener {
    private Button btnEnglish, btnIndonesian;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language);

        btnEnglish = (Button) findViewById(R.id.btnEnglish);
        btnIndonesian = (Button) findViewById(R.id.btnIndonesian);
        btnEnglish.setOnClickListener(this);
        btnIndonesian.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnEnglish:
                Toast.makeText(getApplicationContext(),"Under Construction",Toast.LENGTH_SHORT).show();
                break;
            case R.id.btnIndonesian:
                Toast.makeText(getApplicationContext(),"Under Construction",Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
