package com.kotaksarapan.kotaksarapan;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MessageSuccessfullActivity extends AppCompatActivity {
    private TextView txtMessageTop, txtMessageBottom, txtMessageBottom2, txtButtonText;
    private RelativeLayout submit;

    private String messageFor;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_successfull);

        messageFor = getIntent().getStringExtra("messageFor");
        String message1 = getIntent().getStringExtra("message1");
        String message2 = getIntent().getStringExtra("message2");
        String message3 = getIntent().getStringExtra("message3");

        txtMessageTop = (TextView) findViewById(R.id.txtMessageTop);
        txtMessageBottom = (TextView) findViewById(R.id.txtMessageBottom);
        txtMessageBottom2 = (TextView) findViewById(R.id.txtMessageBottom2);
        submit = (RelativeLayout) findViewById(R.id.submitLayout);
        txtButtonText = (TextView) findViewById(R.id.txtButtonText);

        if (messageFor.equals("registration")){
            txtMessageTop.setText("REGISTRATION \nSUCCESSFULL");
            txtMessageBottom.setText("We will contact you on");
            txtButtonText.setText("Home");
        }else if (messageFor.equals("topup")){
            txtMessageTop.setText("TOP UP SUCCESSFULL");
            txtMessageBottom.setText("Current Balance");
            txtButtonText.setText("Continue to Checkout");
        }else if(messageFor.equals("order")){
            txtMessageTop.setText("ORDER SUCCESSFULL");
            txtMessageBottom.setText("Current Balance");
            txtButtonText.setText("Track Order");
        }

        txtMessageBottom2.setText(message3);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (messageFor.equals("registration")){
                    startActivity(new Intent(MessageSuccessfullActivity.this,MealsDrawerActivity.class));
                    finish();
                }else if (messageFor.equals("topup")){
                    startActivity(new Intent(MessageSuccessfullActivity.this,CheckoutActivity.class));
                    finish();
                }else if (messageFor.equals("order")){
                    startActivity(new Intent(MessageSuccessfullActivity.this,MealsDrawerActivity.class));
                    finish();
                }
            }
        });
    }
}
