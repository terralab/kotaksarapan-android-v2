package com.kotaksarapan.kotaksarapan;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.kotaksarapan.kotaksarapan.Helper.SessionManager;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class BeHomecookActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private EditText txtFullName;
    private EditText txtEmail;
    private EditText txtPhoneNumber;
    private EditText txtFavourite;
    private EditText txtSugest;
    private FloatingActionButton fabNext;
    private TextInputLayout inputEmail;
    private TextInputLayout inputPhoneNumber;
    private SessionManager session;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_be_homecook);

        session = new SessionManager(getApplicationContext());

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.setDrawerIndicatorEnabled(true);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        txtFullName = (EditText) findViewById(R.id.txtFullName);
        txtEmail = (EditText) findViewById(R.id.txtEmail);
        txtPhoneNumber = (EditText) findViewById(R.id.txtPhoneNumber);
        txtFavourite = (EditText) findViewById(R.id.txtFavourite);
        txtSugest = (EditText) findViewById(R.id.txtSugest);
        fabNext = (FloatingActionButton) findViewById(R.id.fabNext);
        inputEmail = (TextInputLayout) findViewById(R.id.inputEmail);
        inputPhoneNumber = (TextInputLayout) findViewById(R.id.inputPhoneNumber);

        txtEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                validateEmail();
            }
        });

        txtPhoneNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                validatePhone();
            }
        });

        fabNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = txtFullName.getText().toString().trim();
                String email = txtEmail.getText().toString().trim();
                String phone = txtPhoneNumber.getText().toString().trim();
                String favourite = txtFavourite.getText().toString().trim();
                String sugest = txtSugest.getText().toString().trim();

                if (!name.isEmpty() && isValidEmail(email) && isValidPhone(phone) && !favourite.isEmpty() && !sugest.isEmpty()){
                    Intent intent = new Intent(BeHomecookActivity.this,MessageSuccessfullActivity.class);
                    intent.putExtra("messageFor","registration");
                    intent.putExtra("message3",email);
                    startActivity(intent);
                }else {
                    Intent intent = new Intent(BeHomecookActivity.this,MessageFailedActivity.class);
                    intent.putExtra("messageFor","registration");
                    startActivity(intent);
                }
            }
        });

    }

    private boolean validateEmail() {
        String email = txtEmail.getText().toString().trim();

        if (email.isEmpty() || !isValidEmail(email)) {
            inputEmail.setError("invalid email");
            requestFocus(txtEmail);
            return false;
        } else {
            inputEmail.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validatePhone() {
        String phone = txtPhoneNumber.getText().toString().trim();

        if (phone.isEmpty() || !isValidPhone(phone)) {
            inputPhoneNumber.setError("invalid number (ex: +62857xxx)");
            requestFocus(txtPhoneNumber);
            return false;
        } else {
            inputPhoneNumber.setErrorEnabled(false);
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
    private static boolean isValidPhone(String phone){
        return !TextUtils.isEmpty(phone) && android.util.Patterns.PHONE.matcher(phone).matches() && phone.contains("+");
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            startActivity(new Intent(BeHomecookActivity.this,MealsDrawerActivity.class));
        } else if (id == R.id.nav_dashboard) {
            if(session.isLoggedIn()){
                startActivity(new Intent(BeHomecookActivity.this, DashboardActivity.class));
            }else{
                startActivity(new Intent(BeHomecookActivity.this,MenuLoginActivity.class));
            }
        } else if (id == R.id.nav_favourite) {
            startActivity(new Intent(BeHomecookActivity.this,FavoritesActivity.class));
        } else if (id == R.id.nav_promotion) {
            startActivity(new Intent(BeHomecookActivity.this,PromotionsActivity.class));
        } else if (id == R.id.nav_about) {
            startActivity(new Intent(BeHomecookActivity.this,AboutUsActivity.class));
        } else if (id == R.id.nav_homecook) {
            startActivity(new Intent(BeHomecookActivity.this,BeHomecookActivity.class));
        } else if (id == R.id.nav_setting) {
            startActivity(new Intent(BeHomecookActivity.this,SettingActivity.class));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
