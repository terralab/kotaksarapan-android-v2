package com.kotaksarapan.kotaksarapan;

import android.app.DialogFragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kotaksarapan.kotaksarapan.Helper.MySQLHelper;
import com.kotaksarapan.kotaksarapan.Helper.SessionManager;
import com.kotaksarapan.kotaksarapan.interfaces.CreateOrderListener;
import com.kotaksarapan.kotaksarapan.model.balance.BalanceResponse;
import com.kotaksarapan.kotaksarapan.model.order.CreateOrderResponse;
import com.kotaksarapan.kotaksarapan.model.sqlite.OrderItemModel;
import com.kotaksarapan.kotaksarapan.rest.loader.BalanceLoader;
import com.kotaksarapan.kotaksarapan.rest.sender.CreateOrderSender;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import okhttp3.MultipartBody;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class CheckoutActivity extends AppCompatActivity implements View.OnClickListener{
    private TextView txtFullName;
    private TextView txtLocation;
    private TextView txtStreet;
    private TextView txtMessage;
    private TextView txtDeliverDate;
    private TextView txtTotalPayment;
    private TextView txtBalance;

    private String addressid;
    private String fullname;
    private String location;
    private String street;
    private String message;
    private String date;
    private int total;
    private String remainingRp;

    private ImageButton btnEditAddress;
    private ImageButton btnEditDate;
    private ImageButton btnEditPayment;

    private RelativeLayout submit;

    private MySQLHelper db;
    private SessionManager session;

    private CreateOrderSender sender;
    private List<OrderItemModel> items;

//    private OrderSender orderSender;
//    private OrderModel orderModel;
//
//    private ArrayList<ListOrderModel> listorder;
//    private ListOrderSender listOrderSender;

    private BalanceLoader balanceLoader;

    private Locale localeID = new Locale("in","ID");
    private NumberFormat rupiah = NumberFormat.getInstance(localeID);

    private FragmentTransaction ft ;
    private DialogFragment loading ;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);

        db = MySQLHelper.getInstance(getApplicationContext());
        session = new SessionManager(getApplicationContext());
        items = new ArrayList<>();

        txtFullName = (TextView) findViewById(R.id.txtFullName);
        txtLocation = (TextView) findViewById(R.id.txtLocation);
        txtStreet = (TextView) findViewById(R.id.txtStreet);
        txtMessage = (TextView) findViewById(R.id.txtMessage);
        txtDeliverDate = (TextView) findViewById(R.id.txtDeliveryDate);
        txtTotalPayment = (TextView) findViewById(R.id.txtTotalPayment);
        txtBalance = (TextView) findViewById(R.id.txtBalance);

        btnEditAddress = (ImageButton) findViewById(R.id.btnEditAddress);
        btnEditDate = (ImageButton) findViewById(R.id.btnEditDate);
        btnEditPayment = (ImageButton) findViewById(R.id.btnEditPayment);
        submit = (RelativeLayout) findViewById(R.id.submitLayout);
        btnEditAddress.setOnClickListener(this);
        btnEditDate.setOnClickListener(this);
        btnEditPayment.setOnClickListener(this);
        submit.setOnClickListener(this);

        getBalance();
        showInfo();
    }

    void showLoading() {
        ft = getFragmentManager().beginTransaction();
        loading = LoadingActivity.newInstance();
        loading.show(ft, "dialog");
    }

    private void getBalance(){
        balanceLoader = new BalanceLoader();
        balanceLoader.load(session.getAuthToken(), new BalanceLoader.BalanceListener() {
            @Override
            public void onComplete(BalanceResponse response) {
                int balance = (int) response.getData().getAttributes().getBalance();
                int remaining = balance - total;
                remainingRp = rupiah.format(remaining);
                txtBalance.setText("Remaining Balance - Rp."+ remainingRp +",-");
            }

            @Override
            public void onError(String m) {

            }
        });
    }

    private void showInfo(){
        Cursor c = db.getOrderInfo(session.getID());
        if(c != null){
            c.moveToFirst();
            addressid = c.getString(c.getColumnIndex(MySQLHelper.COLUMN_ADDRESS_ID));
            fullname = c.getString(c.getColumnIndex(MySQLHelper.COLUMN_FULLNAME));
            location = c.getString(c.getColumnIndex(MySQLHelper.COLUMN_ADDRESS_NAME));
            street = c.getString(c.getColumnIndex(MySQLHelper.COLUMN_ADDRESS));
            message = c.getString(c.getColumnIndex(MySQLHelper.COLUMN_MESSAGE));
            date = c.getString(c.getColumnIndex(MySQLHelper.COLUMN_DELIVERY_DATE));
            total = Integer.parseInt(c.getString(c.getColumnIndex(MySQLHelper.COLUMN_TOTAL)));

            String totalRp = rupiah.format(total);

            DateFormat formatDateDB = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            DateFormat formatDate = new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault());
            Date displayDate = null;
            String strDisplay = null;
            try {
                displayDate = formatDateDB.parse(date);
                strDisplay = formatDate.format(displayDate);
                txtDeliverDate.setText(strDisplay);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            txtFullName.setText(fullname);
            txtLocation.setText(location);
            txtStreet.setText(street);
            txtMessage.setText("''"+message+"''");
            txtTotalPayment.setText("Total - Rp."+totalRp+",-");

            c.close();
        }
    }

    private void sendOrder(){
        DateFormat formatDateDB = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        DateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd hh:mm", Locale.getDefault());
        Date displayDate = null;
        String strDisplay = null;
        try {
            displayDate = formatDateDB.parse(date);
            strDisplay = formatDate.format(displayDate);
            Log.i("TAG", "sendOrder: date parse --> "+strDisplay);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        MultipartBody.Builder mutipartBuilder = new MultipartBody.Builder();
        mutipartBuilder.setType(MultipartBody.FORM);
        mutipartBuilder.addFormDataPart("address_id", addressid);
        mutipartBuilder.addFormDataPart("message", message);
        mutipartBuilder.addFormDataPart("delivery_date", strDisplay);
        mutipartBuilder.addFormDataPart("total", String.valueOf(total));

        items = db.getAllOrderItem();
        for(int i=0; i<items.size(); i++){
            Log.i("TAG", "sendOrder: array order --> "+i);
            Log.i("TAG", "sendOrder: order ke --> "+i+" id --> "+items.get(i).getMealid()+" = "+items.get(i).getMealname());
            mutipartBuilder.addFormDataPart("items["+i+"][id]", items.get(i).getMealid());
            mutipartBuilder.addFormDataPart("items["+i+"][amount]", items.get(i).getAmount());
            mutipartBuilder.addFormDataPart("items["+i+"][note]", "no message");
        }

        MultipartBody multipartBody = mutipartBuilder.build();

        sender = new CreateOrderSender();
        sender.send(session.getAuthToken(), multipartBody, new CreateOrderListener() {
            @Override
            public void onResponse(CreateOrderResponse response) {
                loading.dismiss();
                db.emptyTable();
//                new Gson().toJson(response);
                Intent intent = new Intent(CheckoutActivity.this, MessageSuccessfullActivity.class);
                intent.putExtra("messageFor","order");
                intent.putExtra("message3","Rp. "+ remainingRp +",-");
                startActivity(intent);
            }

            @Override
            public void onFailure(String m) {
                loading.dismiss();
                Intent intent = new Intent(CheckoutActivity.this, MessageFailedActivity.class);
                intent.putExtra("messageFor","order");
                startActivity(intent);
            }
        });
    }

//    private void sendOrder(){
//        orderModel = new OrderModel();
//        orderModel.setUserid(session.getID());
//        orderModel.setAddress(location);
//        orderModel.setAddressextra(street);
//        orderModel.setMessage(message);
//        orderModel.setDeliverydate(date);
//        orderModel.setTotal(total);
//        orderModel.setTotal(credit);
//        orderModel.setLatitude(latitude);
//        orderModel.setLongitude(longitude);
//        orderModel.setPaymentstatus("in-process");
//        orderModel.setIscompleted("0");
//        orderModel.setCouponcode("0");
//        orderModel.setDiscount("0");
//        orderModel.setSubtotal(0);
//
//        orderSender = new OrderSender();
//        orderSender.send(session.getAuthToken(), orderModel,this);
//    }

//    @Override
//    public void onResponse(OrderResponse response) {
//        int orderID = response.getTransaction().get_orderid();
//        Log.i("TAG", "onResponse: send order --> "+orderID);
//        listorder = new ArrayList<>();
//        for(OrderItemModel data : db.getAllOrderItem())
//        {
//            ListOrderModel item = new ListOrderModel();
//            item.set_cookid(data.getCookid());
//            item.set_mealid(Integer.parseInt(data.getMealid()));
//            item.setNotes("test");
//            item.setPrice(data.getPrice());
//            item.setOriginalprice(data.getOriginalprice());
//            item.setAmount(data.getAmount());
//            item.set_orderid(String.valueOf(orderID));
//            listorder.add(item);
//        }
//
//        listOrderSender = new ListOrderSender();
//        listOrderSender.send(session.getAuthToken(), listorder, new ListOrderSender.ListOrderListener() {
//            @Override
//            public void onResponse(OrderItemResponse response) {
//                loading.dismiss();
//                db.emptyTable();
//
//                Intent intent = new Intent(CheckoutActivity.this, MessageSuccessfullActivity.class);
//                intent.putExtra("messageFor","order");
//                intent.putExtra("message3","Rp. 0,-");
//                startActivity(intent);
//            }
//
//            @Override
//            public void onFailure(String m) {
//
//            }
//        });
//    }
//
//    @Override
//    public void onFailure(String m) {
//
//    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnEditAddress:
                startActivity(new Intent(CheckoutActivity.this, DeliveryAddressActivity.class));
                finish();
                break;
            case R.id.btnEditDate:
                startActivity(new Intent(CheckoutActivity.this, DeliveryDateActivity.class));
                finish();
                break;
            case R.id.btnEditPayment:
                startActivity(new Intent(CheckoutActivity.this, PaymentActivity.class));
                finish();
                break;
            case R.id.submitLayout:
                sendOrder();
                showLoading();
                break;
        }
    }
}
