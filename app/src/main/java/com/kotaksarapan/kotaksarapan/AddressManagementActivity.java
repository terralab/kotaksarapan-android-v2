package com.kotaksarapan.kotaksarapan;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.kotaksarapan.kotaksarapan.Helper.SessionManager;
import com.kotaksarapan.kotaksarapan.adapter.AddressAdapter;
import com.kotaksarapan.kotaksarapan.interfaces.AddressListener;
import com.kotaksarapan.kotaksarapan.interfaces.AddressesListener;
import com.kotaksarapan.kotaksarapan.model.address.AddressModel;
import com.kotaksarapan.kotaksarapan.model.address.AddressResponse;
import com.kotaksarapan.kotaksarapan.model.address.AddressesResponse;
import com.kotaksarapan.kotaksarapan.model.address.DataModel;
import com.kotaksarapan.kotaksarapan.rest.loader.UserAddressLoader;
import com.kotaksarapan.kotaksarapan.rest.sender.CreateAddressSender;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class AddressManagementActivity extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {
    private LinearLayoutManager manager;
    private RecyclerView rvAddress;
    private AddressAdapter adapter;
    private List<AddressModel> listAddress;

    private UserAddressLoader loader;

    private FloatingActionButton fabAdd;

    private EditText txtLocation;
    private EditText txtStreet;
    private EditText txtLabel;
    private Button btnSave;

    private String location = "";
    private String street = "";
    private double latitude = 0;
    private double longitude = 0;

    private int PLACE_PICKER_REQUEST = 1;
    private GoogleApiClient mGoogleApiClient;

    private SessionManager session;

    private CreateAddressSender sender;
    private AddressModel address;

    @Override
    protected void onStart() {
        super.onStart();
        if( mGoogleApiClient != null )
            mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        if( mGoogleApiClient != null && mGoogleApiClient.isConnected() ) {
            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address_management);

        session = new SessionManager(getApplicationContext());

        mGoogleApiClient = new GoogleApiClient
                .Builder( this )
                .enableAutoManage( this, 0,  this)
                .addApi( Places.GEO_DATA_API )
                .addApi( Places.PLACE_DETECTION_API )
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener( this)
                .build();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        fabAdd = (FloatingActionButton) findViewById(R.id.fabAdd);
        fabAdd.setOnClickListener(this);

        listAddress = new ArrayList<>();
        adapter = new AddressAdapter(listAddress,this);

        rvAddress = (RecyclerView) findViewById(R.id.rvAddresss);
        manager = new LinearLayoutManager(this);
        rvAddress.setLayoutManager(manager);
        rvAddress.setAdapter(adapter);

        getAddress();
    }

    private void getAddress(){
        loader = new UserAddressLoader();
        loader.load(session.getAuthToken(), new AddressesListener() {
            @Override
            public void onResponse(AddressesResponse response) {
                for(DataModel data : response.getData()){
                    address = new AddressModel();
                    address.setLabel(data.getAttributes().getLabel());
                    address.setName(data.getAttributes().getName());
                    address.setAddress(data.getAttributes().getAddress());
                    address.setAddress_extra(data.getAttributes().getAddress_extra());
                    listAddress.add(address);
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onError(String m) {

            }
        });
    }

    private void saveAddress(){
        address = new AddressModel();
        address.setName(txtLocation.getText().toString().trim());
        address.setAddress(txtStreet.getText().toString().trim());
        address.setLabel(txtLabel.getText().toString().trim());
        address.setLatitude(String.valueOf(latitude));
        address.setLongitude(String.valueOf(longitude));

        sender = new CreateAddressSender();
        sender.send(session.getAuthToken(), address, new AddressListener() {
            @Override
            public void onResponse(AddressResponse response) {
                String label = response.getData().getAttributes().getLabel();
                Toast.makeText(getApplicationContext(),"address "+label+" added",Toast.LENGTH_SHORT).show();
                getAddress();
                adapter.swap(listAddress);
            }

            @Override
            public void onFailure(String m) {

            }
        });
    }

    private void displayPlacePicker() {
        if( mGoogleApiClient == null || !mGoogleApiClient.isConnected() )
            return;

        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

        try {
            startActivityForResult( builder.build( this ), PLACE_PICKER_REQUEST );
        } catch ( GooglePlayServicesRepairableException e ) {
            Log.d( "PlacesAPI Demo", "GooglePlayServicesRepairableException thrown" );
        } catch ( GooglePlayServicesNotAvailableException e ) {
            Log.d( "PlacesAPI Demo", "GooglePlayServicesNotAvailableException thrown" );
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);

                if(place.getAddress().toString().contains("Jakarta")) {
                    location = place.getName().toString();
                    street = place.getAddress().toString();
                    latitude = place.getLatLng().latitude;
                    longitude = place.getLatLng().longitude;
                    Log.i("TAG", "onActivityResult: LatLong --> "+latitude+", "+longitude);
                    txtLocation.setText(location);
                    txtStreet.setText(street);
                    validate();
                }else{
                    txtLocation.setText(null);
                    txtStreet.setText(null);
                    validate();
                    Toast.makeText(getApplicationContext(),"layanan ini baru tersedia di area Jakarta",Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private void validate(){
        if(!txtLocation.getText().toString().trim().isEmpty() && !txtStreet.getText().toString().trim().isEmpty() && !txtLabel.getText().toString().trim().isEmpty()){
            btnSave.setVisibility(View.VISIBLE);
        }else {
            btnSave.setVisibility(View.GONE);
        }
    }

    private void addAddressDialog(){
        final Dialog dialog = new Dialog(this, R.style.custom_dialog);
        dialog.setContentView(R.layout.dialog_add_address);
        dialog.setTitle("ADD ADDRESS");

        txtLocation = (EditText) dialog.findViewById(R.id.txtLocation);
        txtStreet = (EditText) dialog.findViewById(R.id.txtStreet);
        txtLabel = (EditText) dialog.findViewById(R.id.txtLabel);
        btnSave = (Button) dialog.findViewById(R.id.btnSave);

        txtLabel.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                validate();
            }
        });

        txtLocation.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    displayPlacePicker();
                }
                return true;
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveAddress();
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.fabAdd:
                addAddressDialog();
                break;
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
