package com.kotaksarapan.kotaksarapan;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.kotaksarapan.kotaksarapan.Helper.MySQLHelper;
import com.kotaksarapan.kotaksarapan.Helper.SessionManager;
import com.kotaksarapan.kotaksarapan.model.balance.BalanceResponse;
import com.kotaksarapan.kotaksarapan.rest.loader.BalanceLoader;

import java.text.NumberFormat;
import java.util.Locale;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class PaymentActivity extends AppCompatActivity implements View.OnClickListener{
    private TextView txtTotalPayment, txtCredit, txtPromoCode;
    private Button btnTopUp;
    private FloatingActionButton fabCart, fabNext;

    private MySQLHelper db;
    private SessionManager session;

    private Integer totalPayment = 0;
    private Integer currentCredit = 0;

    private Locale localeID = new Locale("in","ID");
    private NumberFormat rupiah = NumberFormat.getInstance(localeID);

    private BalanceLoader balanceLoader;

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        db = MySQLHelper.getInstance(getApplicationContext());
        session = new SessionManager(getApplicationContext());

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        txtTotalPayment = (TextView) findViewById(R.id.txtTotalPayment);
        txtCredit = (TextView) findViewById(R.id.txtCredit);
        txtPromoCode = (TextView) findViewById(R.id.txtPromoCode);
        btnTopUp = (Button) findViewById(R.id.btnTopUp);
        fabCart = (FloatingActionButton) findViewById(R.id.fabCart);
        fabNext = (FloatingActionButton) findViewById(R.id.fabNext);

        getTotal();

        btnTopUp.setOnClickListener(this);
        fabNext.setOnClickListener(this);

        checkCart();
        getBalance();
        validateComplete();
    }

    private void getBalance(){
        balanceLoader = new BalanceLoader();
        balanceLoader.load(session.getAuthToken(), new BalanceLoader.BalanceListener() {
            @Override
            public void onComplete(BalanceResponse response) {
                currentCredit = (int) response.getData().getAttributes().getBalance();
                String balance = rupiah.format(response.getData().getAttributes().getBalance());
                txtCredit.setText("Rp. "+balance+",-");
                validateComplete();
            }

            @Override
            public void onError(String m) {

            }
        });
    }

    private void getTotal(){
        Cursor c = db.getOrderInfo(session.getID());
        if(c != null) {
            c.moveToFirst();
            int total = c.getInt(c.getColumnIndex(MySQLHelper.COLUMN_TOTAL));
            totalPayment = total;
            String totalrp = rupiah.format(totalPayment);
            txtTotalPayment.setText("Rp. "+totalrp+",-");
            c.close();
        }
    }

    public void checkCart(){
        if(db.isEmptyItem()){
            fabCart.setVisibility(View.GONE);
        }else {
            fabCart.setVisibility(View.VISIBLE);
        }
    }

    private void validateComplete(){
        if(currentCredit >= totalPayment){
            setActivityBackgroundColor(0xfffec713);
            fabNext.setVisibility(View.VISIBLE);
        }else{
            setActivityBackgroundColor(0xfffbfbfb);
            fabNext.setVisibility(View.GONE);
        }
    }

    public void setActivityBackgroundColor(int color) {
        View view = this.getWindow().getDecorView();
        view.setBackgroundColor(color);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnTopUp:
                startActivity(new Intent(PaymentActivity.this, TopUpActivity.class));
                break;
            case R.id.fabNext:
                startActivity(new Intent(PaymentActivity.this,CheckoutActivity.class));
                break;
        }
    }
}
