package com.kotaksarapan.kotaksarapan;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.kotaksarapan.kotaksarapan.Helper.AppHelper;
import com.kotaksarapan.kotaksarapan.Helper.MySQLHelper;
import com.kotaksarapan.kotaksarapan.Helper.SessionManager;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SettingActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,View.OnClickListener{
    private Button btnVersion;
    private Button btnTutorial;
    private Button btnSupport;
    private Button btnPolicy;
    private Button btnLanguage;
    private Button btnRate;
    private Button btnLogOut;

    private MaterialSearchView searchView;

    private AppHelper mApplication;

    private SessionManager session;
    private MySQLHelper db;

    private GoogleApiClient mGoogleApiClient;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        mGoogleApiClient = AppHelper.getGoogleApiHelper().getGoogleApiClient();

        session = new SessionManager(getApplicationContext());
        db = MySQLHelper.getInstance(getApplicationContext());

        mApplication = (AppHelper) getApplication();
        mApplication.setContext(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.setDrawerIndicatorEnabled(true);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        btnVersion = (Button) findViewById(R.id.btnVersion);
        btnTutorial = (Button) findViewById(R.id.btnTutorial);
        btnSupport = (Button) findViewById(R.id.btnSupport);
        btnPolicy = (Button) findViewById(R.id.btnPolicy);
        btnLanguage = (Button) findViewById(R.id.btnLanguage);
        btnRate = (Button) findViewById(R.id.btnRate);
        btnLogOut = (Button) findViewById(R.id.btnLogOut);

        btnVersion.setOnClickListener(this);
        btnTutorial.setOnClickListener(this);
        btnSupport.setOnClickListener(this);
        btnPolicy.setOnClickListener(this);
        btnLanguage.setOnClickListener(this);
        btnRate.setOnClickListener(this);
        btnLogOut.setOnClickListener(this);

        searchView = (MaterialSearchView) findViewById(R.id.search_view);
        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                //Do some magic
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                //Do some magic
                return false;
            }
        });

        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {
                //Do some magic
            }

            @Override
            public void onSearchViewClosed() {
                //Do some magic
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MaterialSearchView.REQUEST_VOICE && resultCode == RESULT_OK) {
            ArrayList<String> matches = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            if (matches != null && matches.size() > 0) {
                String searchWrd = matches.get(0);
                if (!TextUtils.isEmpty(searchWrd)) {
                    searchView.setQuery(searchWrd, false);
                }
            }

            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search, menu);

        MenuItem item = menu.findItem(R.id.search);
        searchView.setMenuItem(item);

        return true;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.search) {
            //todo add action here
            return true;
        }

        return super.onOptionsItemSelected(item);
    }*/

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            startActivity(new Intent(SettingActivity.this,MealsDrawerActivity.class));
        } else if (id == R.id.nav_dashboard) {
            if(session.isLoggedIn()){
                startActivity(new Intent(SettingActivity.this, DashboardActivity.class));
            }else{
                startActivity(new Intent(SettingActivity.this,MenuLoginActivity.class));
            }
        } else if (id == R.id.nav_favourite) {
            startActivity(new Intent(SettingActivity.this,FavoritesActivity.class));
        } else if (id == R.id.nav_promotion) {
            startActivity(new Intent(SettingActivity.this,PromotionsActivity.class));
        } else if (id == R.id.nav_about) {
            startActivity(new Intent(SettingActivity.this,AboutUsActivity.class));
        } else if (id == R.id.nav_homecook) {
            startActivity(new Intent(SettingActivity.this,BeHomecookActivity.class));
        } else if (id == R.id.nav_setting) {
            startActivity(new Intent(SettingActivity.this,SettingActivity.class));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void signOutGoogle() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        AppHelper.getGoogleApiHelper().disconnect();
                    }
                });
    }
    private void revokeAccess() {
        Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        AppHelper.getGoogleApiHelper().disconnect();
                    }
                });
    }

    public void doSignOut(){
        session.logoutUser();
        db.emptyTable();
        LoginManager.getInstance().logOut();

        Toast.makeText(getApplicationContext(),"User successfully signed-out!", Toast.LENGTH_SHORT).show();
        startActivity(new Intent(this,MealsDrawerActivity.class));
        finish();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnVersion:
                Toast.makeText(getApplicationContext(),"Under Construction", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btnTutorial:
                Toast.makeText(getApplicationContext(),"Under Construction", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btnSupport:
                Toast.makeText(getApplicationContext(),"Under Construction", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btnPolicy:
                Toast.makeText(getApplicationContext(),"Under Construction", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btnLanguage:
                Toast.makeText(getApplicationContext(),"Under Construction", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btnRate:
                Toast.makeText(getApplicationContext(),"Under Construction", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btnLogOut:
                doSignOut();
                break;
        }
    }
}
