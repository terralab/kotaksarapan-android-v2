package com.kotaksarapan.kotaksarapan;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.kotaksarapan.kotaksarapan.Helper.EndlessRecyclerViewListener;
import com.kotaksarapan.kotaksarapan.Helper.MySQLHelper;
import com.kotaksarapan.kotaksarapan.Helper.SessionManager;
import com.kotaksarapan.kotaksarapan.adapter.MealAdapter;
import com.kotaksarapan.kotaksarapan.interfaces.OnCartClick;
import com.kotaksarapan.kotaksarapan.model.ListMeals.DataModel;
import com.kotaksarapan.kotaksarapan.model.ListMeals.MealsResponse;
import com.kotaksarapan.kotaksarapan.model.Meal.MealModel;
import com.kotaksarapan.kotaksarapan.rest.loader.MealsLoader;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class MealsFragment extends Fragment implements MealsLoader.MealsListener, OnCartClick {
    public static final String TITLE = "";
    public static final String ID = "";

    private SessionManager session;
    private MySQLHelper db;

    private LinearLayoutManager manager;
    private RecyclerView rvMeals;
    private MealAdapter adapter;

    private List<MealModel> listMeal;
    private MealsLoader loader;

    private int id;
//    private int currentPage =1;
    private int totalPages;
//    private String next;

    private EndlessRecyclerViewListener scrollListener;

    public MealsFragment() {
        // Required empty public constructor
    }

    public static final MealsFragment newInstance(int id, String title){
        MealsFragment f = new MealsFragment();
        Bundle bundle = new Bundle(1);
        bundle.putInt(ID, id);
//        bundle.putString(TITLE, title);
        f.setArguments(bundle);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        id = getArguments().getInt(ID);
        db = MySQLHelper.getInstance(getActivity());
//        String title = getArguments().getString(TITLE);
        View view = inflater.inflate(R.layout.fragment_meals, container, false);
        Log.i("TAG", "onCreateView: meals fragment extra message --> "+id+" <--id || title --> ");

        session = new SessionManager(getActivity());

        listMeal = new ArrayList<>();
        manager = new LinearLayoutManager(getActivity());
        adapter = new MealAdapter(listMeal, getActivity(), this);

        scrollListener = new EndlessRecyclerViewListener(manager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                page = page+1;
                if (page <= totalPages) {
                    getMeals(page);
                }
                else{
                    Toast.makeText(getActivity(), "semua data sudah di load", Toast.LENGTH_SHORT).show();
                }
            }
        };

        rvMeals = (RecyclerView) view.findViewById(R.id.rvMeals);
        rvMeals.setAdapter(adapter);
        rvMeals.setLayoutManager(manager);
        rvMeals.addOnScrollListener(scrollListener);

        getMeals(1);
        ((MealsDrawerActivity)getActivity()).checkCart();

        return view;
    }

    private void getMeals(int page){
        loader = new MealsLoader();
        loader.load(session.getAuthToken(),id,page,this);
    }

    @Override
    public void onComplete(MealsResponse data) {
//        currentPage = data.getMeta().getPagination().getCurrent_page();
        totalPages = data.getMeta().getPagination().getTotal_pages();
//        next = data.getLinks().getNext();
//        Log.i("TAG", "onComplete: page current --> "+currentPage);
        Log.i("TAG", "onComplete: page total --> "+totalPages);
//        Log.i("TAG", "onComplete: page next url --> "+next);

        for (DataModel meals : data.getData())
        {
            MealModel meal = new MealModel();
            meal.setId(meals.getId());
            meal.set_cookid(meals.getAttributes().get_cookid());
            meal.setTitle(meals.getAttributes().getTitle());
            meal.setImagesmall(meals.getAttributes().getImagesmall());
            meal.setImagecook(meals.getAttributes().getCook().getImagecook());
            meal.setSummary(meals.getAttributes().getSummary());
            meal.setPrice(meals.getAttributes().getPrice());
            meal.setOriginalprice(meals.getAttributes().getOriginalprice());
            meal.setRating(meals.getAttributes().getRating());
//            meal.setCategoryId(meals.getAttributes().getCategoryId());
            meal.set_cookid(meals.getAttributes().get_cookid());
            meal.setIsfavorite(meals.getAttributes().isfavorite());
            meal.setIsavailable(meals.getAttributes().isavailable());
            meal.setFullname(meals.getAttributes().getCook().getFullname());
            meal.setNickname(meals.getAttributes().getCook().getNickname());
            listMeal.add(meal);
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onError(String m) {

    }

    @Override
    public void onClickCart(String cookID, String mealID, String mealName, int price, int originalPrice, int amount) {
        if(db.isItemExist(mealID)){
            Toast.makeText(getActivity(),"already in cart",Toast.LENGTH_SHORT).show();
        }else {
            db.addItem(cookID, mealID, mealName, price, originalPrice, amount);
            ((MealsDrawerActivity) getActivity()).checkCart();
        }
    }
}
