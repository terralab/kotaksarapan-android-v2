package com.kotaksarapan.kotaksarapan;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.RelativeLayout;

import com.kotaksarapan.kotaksarapan.Helper.SessionManager;
import com.kotaksarapan.kotaksarapan.adapter.TopUpAdapter;
import com.kotaksarapan.kotaksarapan.Helper.TopUpModel;
import com.kotaksarapan.kotaksarapan.interfaces.OnClickTopUp;
import com.kotaksarapan.kotaksarapan.interfaces.TopUpListener;
import com.kotaksarapan.kotaksarapan.model.topup.TopUpCreditModel;
import com.kotaksarapan.kotaksarapan.model.topup.TopUpResponse;
import com.kotaksarapan.kotaksarapan.rest.sender.TopUpSender;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class TopUpActivity extends AppCompatActivity implements View.OnClickListener, View.OnTouchListener, OnClickTopUp {
    private FloatingActionButton fabCart;
    private FloatingActionButton fabNext;

    private AutoCompleteTextView txtPayment;
    private String payment[]={"Bank Transfer", "Credit Card"};

    private RelativeLayout paymentDetailLayout;

    private GridLayoutManager manager;
    private RecyclerView rvAmount;
    private TopUpAdapter adapter;
    private List<TopUpModel> topupList;

    private float amountSelected;

    private TopUpSender sender;
    private TopUpCreditModel topup;

    private SessionManager session;

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_top_up);

        session = new SessionManager(getApplicationContext());

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        txtPayment = (AutoCompleteTextView) findViewById(R.id.txtPayment);
        rvAmount = (RecyclerView) findViewById(R.id.rvMeals);
        fabCart = (FloatingActionButton) findViewById(R.id.fabCart);
        fabNext = (FloatingActionButton) findViewById(R.id.fabNext);
        paymentDetailLayout = (RelativeLayout) findViewById(R.id.paymentDetailLayout);

        ArrayAdapter<String> paymentAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, payment);
        txtPayment.setAdapter(paymentAdapter);
        txtPayment.setOnTouchListener(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            txtPayment.setOnDismissListener(new AutoCompleteTextView.OnDismissListener() {
                @Override
                public void onDismiss() {
                    validateCC();
                }
            });
        }

        fabCart.setOnClickListener(this);
        fabNext.setOnClickListener(this);
        txtPayment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                validateCC();
            }
        });

        topupList = new ArrayList<>();
        adapter = new TopUpAdapter(topupList, getApplicationContext(), this);

        rvAmount = (RecyclerView) findViewById(R.id.rvAmount);
        manager = new GridLayoutManager(getApplicationContext(),2);
        rvAmount.setLayoutManager(manager);
        rvAmount.setAdapter(adapter);

        setCreditAmount();

    }

    private void doTopUp(){
        topup = new TopUpCreditModel();
        topup.setAmount(amountSelected);
        topup.setMessage(txtPayment.getText().toString().trim());

        sender = new TopUpSender();
        sender.send(session.getAuthToken(), topup, new TopUpListener() {
            @Override
            public void onResponse(TopUpResponse response) {
//                Toast.makeText(getApplicationContext(),response.getData().getAttributes().getStatus(),Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(TopUpActivity.this,MessageSuccessfullActivity.class);
                intent.putExtra("messageFor","topup");
                intent.putExtra("message3","Rp. 0,-");
                startActivity(intent);
            }

            @Override
            public void onFailure(String m) {

            }
        });
    }

    private void validateComplete(){
        if(!txtPayment.getText().toString().trim().isEmpty()){
            fabNext.setVisibility(View.VISIBLE);
        }else {
            fabNext.setVisibility(View.GONE);
        }
    }

    private void validateCC(){
        if(txtPayment.getText().toString().trim().contains("Credit Card")){
            paymentDetailLayout.setVisibility(View.VISIBLE);
        }else {
            paymentDetailLayout.setVisibility(View.GONE);
        }
        validateComplete();
    }

    private void setCreditAmount() {
        Context c = getApplicationContext();

        TopUpModel amount = new TopUpModel(50000);
        topupList.add(amount);

        amount = new TopUpModel(100000);
        topupList.add(amount);

        amount = new TopUpModel(200000);
        topupList.add(amount);

        amount = new TopUpModel(500000);
        topupList.add(amount);

        adapter.notifyDataSetChanged();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.fabCart:

                break;
            case R.id.fabNext:
                doTopUp();
                break;
        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        txtPayment.showDropDown();
        return false;
    }

    @Override
    public void onClickCreditAmount(float amount) {
        amountSelected = amount;
        Log.i("TAG", "onClickCreditAmount: "+ amountSelected);
    }
}
