package com.kotaksarapan.kotaksarapan;

import android.app.DialogFragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.kotaksarapan.kotaksarapan.Helper.MySQLHelper;
import com.kotaksarapan.kotaksarapan.Helper.SessionManager;
import com.kotaksarapan.kotaksarapan.adapter.MealAdapter;
import com.kotaksarapan.kotaksarapan.interfaces.OnCartClick;
import com.kotaksarapan.kotaksarapan.model.Cook.CookResponse;
import com.kotaksarapan.kotaksarapan.model.ListMeals.DataModel;
import com.kotaksarapan.kotaksarapan.model.ListMeals.MealsResponse;
import com.kotaksarapan.kotaksarapan.model.Meal.MealModel;
import com.kotaksarapan.kotaksarapan.rest.loader.CookLoader;
import com.kotaksarapan.kotaksarapan.rest.loader.CookMealsLoader;
import com.kotaksarapan.kotaksarapan.rest.loader.MealsLoader;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class HomecookProfileActivity extends AppCompatActivity implements OnCartClick, View.OnClickListener {
    private FloatingActionButton fabCart;
    private TextView txtName;
    private TextView txtAddress;
    private TextView txtSince;
    private TextView txtDescription;
    private CircleImageView imageCook;

    private SessionManager session;
    private MySQLHelper db;

    private CookLoader cookLoader;
    private CookMealsLoader cookMealsLoader;

    private LinearLayoutManager manager;
    private RecyclerView rvMeals;
    private MealAdapter adapter;
    private List<MealModel> listMeal;

    private int id;

    private FragmentTransaction ft ;
    private DialogFragment loading ;

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homecook_profile);

        session = new SessionManager(getApplicationContext());
        db = MySQLHelper.getInstance(getApplicationContext());

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        fabCart = (FloatingActionButton) findViewById(R.id.fabCart);
        txtName = (TextView) findViewById(R.id.txtName);
        txtAddress = (TextView) findViewById(R.id.txtAddress);
        txtSince = (TextView) findViewById(R.id.txtSince);
        txtDescription = (TextView) findViewById(R.id.txtDescription);
        imageCook = (CircleImageView) findViewById(R.id.imageCook);

        fabCart.setOnClickListener(this);

        showDialog();

        listMeal = new ArrayList<>();
        adapter = new MealAdapter(listMeal, this, this);

        rvMeals = (RecyclerView) findViewById(R.id.rvMeals);
        manager = new LinearLayoutManager(this);
        rvMeals.setLayoutManager(manager);
        rvMeals.setAdapter(adapter);

        id = getIntent().getIntExtra("id",0);
        getCook(id);
        getCookMeals(id);

        checkCart();

    }

    private void getCook(int id){
        cookLoader = new CookLoader();
        cookLoader.load(session.getAuthToken(),id, new CookLoader.CookListener() {
            @Override
            public void onComplete(CookResponse data) {
//                loading.dismiss();
                txtName.setText(data.getData().getAttributes().getNickname());
                txtAddress.setText(data.getData().getAttributes().getAddressextra());
                txtSince.setText(data.getData().getAttributes().getSince());
                txtDescription.setText(data.getData().getAttributes().getDescription());

            }

            @Override
            public void onError(String m) {

            }
        });
    }

    private void getCookMeals(int id){
        cookMealsLoader = new CookMealsLoader();
        cookMealsLoader.load(session.getAuthToken(),id, new MealsLoader.MealsListener() {
            @Override
            public void onComplete(MealsResponse data) {
                loading.dismiss();
                for (DataModel dat : data.getData())
                {
                    MealModel meal = new MealModel();
                    meal.setId(dat.getId());
                    meal.set_cookid(dat.getAttributes().get_cookid());
                    meal.setTitle(dat.getAttributes().getTitle());
                    meal.setImagesmall(dat.getAttributes().getImagesmall());
                    meal.setImagecook(dat.getAttributes().getCook().getImagecook());
                    meal.setSummary(dat.getAttributes().getSummary());
                    meal.setPrice(dat.getAttributes().getPrice());
                    meal.setRating(dat.getAttributes().getRating());
//                    meal.setCategoryId(dat.getAttributes().getCategoryId());
                    meal.set_cookid(dat.getAttributes().get_cookid());
                    meal.setIsfavorite(dat.getAttributes().isfavorite());
                    meal.setIsavailable(dat.getAttributes().isavailable());
                    meal.setFullname(dat.getAttributes().getCook().getFullname());
                    meal.setNickname(dat.getAttributes().getCook().getNickname());
                    listMeal.add(meal);
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onError(String m) {

            }
        });
    }

    void showDialog() {
        ft = getFragmentManager().beginTransaction();
        loading = LoadingActivity.newInstance();
        loading.show(ft, "dialog");
    }

    public void checkCart(){
        if(db.isEmptyItem()){
            fabCart.setVisibility(View.GONE);
        }else {
            fabCart.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClickCart(String cookID, String mealID, String mealName, int price, int originalPrice, int amount) {
        if(db.isItemExist(mealID)){
            Toast.makeText(getApplicationContext(),"already in cart",Toast.LENGTH_SHORT).show();
        }else {
            db.addItem(cookID, mealID, mealName, price, originalPrice, amount);
            checkCart();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.fabCart:
                startActivity(new Intent(HomecookProfileActivity.this,CartActivity.class));
                break;
        }
    }
}
