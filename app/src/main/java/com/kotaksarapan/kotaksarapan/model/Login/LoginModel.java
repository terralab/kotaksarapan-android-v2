package com.kotaksarapan.kotaksarapan.model.Login;

/**
 * Created by wresniwahyu on 24/07/2017.
 */

public class LoginModel {
    private String email;
    private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
