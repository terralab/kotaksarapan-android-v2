package com.kotaksarapan.kotaksarapan.model.side;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by wresniwahyu on 14/09/2017.
 */

public class SideModel implements Parcelable {
    private int sideid;
    private String side;
    private int price;
    private int amount;

    public SideModel() {
    }

    protected SideModel(Parcel in) {
        sideid = in.readInt();
        side = in.readString();
        price = in.readInt();
        amount = in.readInt();
    }

    public static final Creator<SideModel> CREATOR = new Creator<SideModel>() {
        @Override
        public SideModel createFromParcel(Parcel in) {
            return new SideModel(in);
        }

        @Override
        public SideModel[] newArray(int size) {
            return new SideModel[size];
        }
    };

    public int getSideid() {
        return sideid;
    }

    public void setSideid(int sideid) {
        this.sideid = sideid;
    }

    public String getSide() {
        return side;
    }

    public void setSide(String side) {
        this.side = side;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(sideid);
        parcel.writeString(side);
        parcel.writeInt(price);
        parcel.writeInt(amount);
    }
}
