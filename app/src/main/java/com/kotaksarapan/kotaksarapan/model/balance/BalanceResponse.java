package com.kotaksarapan.kotaksarapan.model.balance;

/**
 * Created by wresniwahyu on 18/09/2017.
 */

public class BalanceResponse {
    private DataModel data;

    public DataModel getData() {
        return data;
    }

    public void setData(DataModel data) {
        this.data = data;
    }
}
