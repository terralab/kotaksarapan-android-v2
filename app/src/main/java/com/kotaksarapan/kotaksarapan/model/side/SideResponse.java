package com.kotaksarapan.kotaksarapan.model.side;

import java.util.List;

/**
 * Created by wresniwahyu on 14/09/2017.
 */

public class SideResponse {
    private List<DataModel> data;

    public List<DataModel> getData() {
        return data;
    }

    public void setData(List<DataModel> data) {
        this.data = data;
    }
}
