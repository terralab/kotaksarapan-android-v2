package com.kotaksarapan.kotaksarapan.model.Meal;

/**
 * Created by wresniwahyu on 5/31/2017.
 */

public class DataModel {
    private String type;
    private String id;
    private AttributesModel attributes;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public AttributesModel getAttributes() {
        return attributes;
    }

    public void setAttributes(AttributesModel attributes) {
        this.attributes = attributes;
    }
}
