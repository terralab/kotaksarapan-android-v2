package com.kotaksarapan.kotaksarapan.model.balance;

/**
 * Created by wresniwahyu on 18/09/2017.
 */

public class AttributesModel {
    private float credit;
    private float debt;
    private float balance;

    public float getCredit() {
        return credit;
    }

    public void setCredit(float credit) {
        this.credit = credit;
    }

    public float getDebt() {
        return debt;
    }

    public void setDebt(float debt) {
        this.debt = debt;
    }

    public float getBalance() {
        return balance;
    }

    public void setBalance(float balance) {
        this.balance = balance;
    }
}
