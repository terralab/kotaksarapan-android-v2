package com.kotaksarapan.kotaksarapan.model.topup;

/**
 * Created by wresniwahyu on 18/09/2017.
 */

public class TopUpCreditModel {
    private float amount;
    private String message;

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
