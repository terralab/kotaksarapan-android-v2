package com.kotaksarapan.kotaksarapan.model.ListMeals;

/**
 * Created by wresniwahyu on 5/22/2017.
 */

public class CookModel {
    private int id;
    private String uuid;
    private String fullname;
    private String nickname;
    private String description;
    private String addressextra;
    private String since;
    private String imagecook;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddressextra() {
        return addressextra;
    }

    public void setAddressextra(String addressextra) {
        this.addressextra = addressextra;
    }

    public String getSince() {
        return since;
    }

    public void setSince(String since) {
        this.since = since;
    }

    public String getImagecook() {
        return imagecook;
    }

    public void setImagecook(String imagecook) {
        this.imagecook = imagecook;
    }
}
