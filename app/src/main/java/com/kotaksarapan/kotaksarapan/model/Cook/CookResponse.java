package com.kotaksarapan.kotaksarapan.model.Cook;

/**
 * Created by wresniwahyu on 6/5/2017.
 */

public class CookResponse {
    private DataModel data;

    public DataModel getData() {
        return data;
    }

    public void setData(DataModel data) {
        this.data = data;
    }
}
