package com.kotaksarapan.kotaksarapan.model.topup;

/**
 * Created by wresniwahyu on 18/09/2017.
 */

public class AttributesModel {
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
