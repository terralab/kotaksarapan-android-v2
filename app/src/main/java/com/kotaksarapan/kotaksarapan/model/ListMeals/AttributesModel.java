package com.kotaksarapan.kotaksarapan.model.ListMeals;

/**
 * Created by wresniwahyu on 5/22/2017.
 */

public class AttributesModel {
    private int _cookid;
    private String title;
    private String imagesmall;
    private String summary;
    private String description;
    private boolean isfavorite;
    private boolean isavailable;
    private int price;
    private int originalprice;
    private Float rating;
    private CookModel cook;

    public int get_cookid() {
        return _cookid;
    }

    public void set_cookid(int _cookid) {
        this._cookid = _cookid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImagesmall() {
        return imagesmall;
    }

    public void setImagesmall(String imagesmall) {
        this.imagesmall = imagesmall;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isfavorite() {
        return isfavorite;
    }

    public void setIsfavorite(boolean isfavorite) {
        this.isfavorite = isfavorite;
    }

    public boolean isavailable() {
        return isavailable;
    }

    public void setIsavailable(boolean isavailable) {
        this.isavailable = isavailable;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getOriginalprice() {
        return originalprice;
    }

    public void setOriginalprice(int originalprice) {
        this.originalprice = originalprice;
    }

    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }

    public CookModel getCook() {
        return cook;
    }

    public void setCook(CookModel cook) {
        this.cook = cook;
    }
}
