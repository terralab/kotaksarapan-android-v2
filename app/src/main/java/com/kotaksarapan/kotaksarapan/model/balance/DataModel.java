package com.kotaksarapan.kotaksarapan.model.balance;

/**
 * Created by wresniwahyu on 18/09/2017.
 */

public class DataModel {
    private int id;
    private AttributesModel attributes;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public AttributesModel getAttributes() {
        return attributes;
    }

    public void setAttributes(AttributesModel attributes) {
        this.attributes = attributes;
    }
}
