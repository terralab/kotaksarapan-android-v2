package com.kotaksarapan.kotaksarapan.model.order;

/**
 * Created by wresniwahyu on 28/08/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ListOrderModel {
    private String _cookid;
    private Integer _mealid;
    private String notes;
    private String price;
    private String originalprice;
    private String amount;
    private String _orderid;

    public String get_cookid() {
        return _cookid;
    }

    public void set_cookid(String _cookid) {
        this._cookid = _cookid;
    }

    public Integer get_mealid() {
        return _mealid;
    }

    public void set_mealid(Integer _mealid) {
        this._mealid = _mealid;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getOriginalprice() {
        return originalprice;
    }

    public void setOriginalprice(String originalprice) {
        this.originalprice = originalprice;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String get_orderid() {
        return _orderid;
    }

    public void set_orderid(String _orderid) {
        this._orderid = _orderid;
    }
}