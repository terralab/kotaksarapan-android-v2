package com.kotaksarapan.kotaksarapan.model.Login;

/**
 * Created by wresniwahyu on 20/07/2017.
 */

public class FbLoginModel {
    private String email;
    private String fb_token;
    private String name;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFb_token() {
        return fb_token;
    }

    public void setFb_token(String fb_token) {
        this.fb_token = fb_token;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
