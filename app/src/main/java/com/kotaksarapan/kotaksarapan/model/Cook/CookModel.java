package com.kotaksarapan.kotaksarapan.model.Cook;

/**
 * Created by wresniwahyu on 6/5/2017.
 */

public class CookModel {
    private String id;
    private String fullname;
    private String nickname;
    private String description;
    private String addressextra;
    private String since;

    public CookModel() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddressextra() {
        return addressextra;
    }

    public void setAddressextra(String addressextra) {
        this.addressextra = addressextra;
    }

    public String getSince() {
        return since;
    }

    public void setSince(String since) {
        this.since = since;
    }
}
