package com.kotaksarapan.kotaksarapan.model.order;

import java.util.List;

/**
 * Created by wresniwahyu on 25/09/2017.
 */

public class CreateOrderModel {
    private int address_id;
    private String message;
    private String delivery_date;
    private int total;
    private List<ItemsModel> items;
    private List<SidesModel> sides;

    public int getAddress_id() {
        return address_id;
    }

    public void setAddress_id(int address_id) {
        this.address_id = address_id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDelivery_date() {
        return delivery_date;
    }

    public void setDelivery_date(String delivery_date) {
        this.delivery_date = delivery_date;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<ItemsModel> getItems() {
        return items;
    }

    public void setItems(List<ItemsModel> items) {
        this.items = items;
    }

    public List<SidesModel> getSides() {
        return sides;
    }

    public void setSides(List<SidesModel> sides) {
        this.sides = sides;
    }
}
