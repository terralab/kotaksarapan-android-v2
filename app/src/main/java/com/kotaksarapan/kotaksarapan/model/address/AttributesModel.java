package com.kotaksarapan.kotaksarapan.model.address;

/**
 * Created by wresniwahyu on 19/09/2017.
 */

public class AttributesModel {
    private String uuid;
    private String name;
    private String label;
    private String address;
    private String address_extra;
    private String longitude;
    private String latitude;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress_extra() {
        return address_extra;
    }

    public void setAddress_extra(String address_extra) {
        this.address_extra = address_extra;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }
}
