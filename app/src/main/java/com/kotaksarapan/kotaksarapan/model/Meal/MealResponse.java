package com.kotaksarapan.kotaksarapan.model.Meal;

/**
 * Created by wresniwahyu on 5/31/2017.
 */

public class MealResponse {
    private DataModel data;

    public DataModel getData() {
        return data;
    }

    public void setData(DataModel data) {
        this.data = data;
    }
}
