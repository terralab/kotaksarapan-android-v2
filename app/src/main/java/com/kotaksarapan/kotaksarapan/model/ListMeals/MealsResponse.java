package com.kotaksarapan.kotaksarapan.model.ListMeals;

import java.util.List;

/**
 * Created by wresniwahyu on 5/22/2017.
 */

public class MealsResponse {
    private List<DataModel> data;
//    private List<IncludedModel> included;
    private MetaModel meta;
    private LinksModel links;

    public MetaModel getMeta() {
        return meta;
    }

    public void setMeta(MetaModel meta) {
        this.meta = meta;
    }

    public LinksModel getLinks() {
        return links;
    }

    public void setLinks(LinksModel links) {
        this.links = links;
    }

   /* public List<IncludedModel> getIncluded() {
        return included;
    }

    public void setIncluded(List<IncludedModel> included) {
        this.included = included;
    }*/

    public List<DataModel> getData() {
        return data;
    }

    public void setData(List<DataModel> data) {
        this.data = data;
    }
}
