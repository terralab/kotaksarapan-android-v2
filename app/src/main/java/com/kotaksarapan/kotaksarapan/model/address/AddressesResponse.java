package com.kotaksarapan.kotaksarapan.model.address;

import java.util.List;

/**
 * Created by wresniwahyu on 20/09/2017.
 */

public class AddressesResponse {
    private List<DataModel> data;

    public List<DataModel> getData() {
        return data;
    }

    public void setData(List<DataModel> data) {
        this.data = data;
    }
}
