package com.kotaksarapan.kotaksarapan.model.order;

import java.io.Serializable;

/**
 * Created by wresniwahyu on 24/08/2017.
 */

public class OrderResponse implements Serializable{
    private boolean success;
    private String message;
    private TransactionModel transaction;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public TransactionModel getTransaction() {
        return transaction;
    }

    public void setTransaction(TransactionModel transaction) {
        this.transaction = transaction;
    }
}
