package com.kotaksarapan.kotaksarapan.model.order;

import java.io.Serializable;

/**
 * Created by wresniwahyu on 24/08/2017.
 */

public class TransactionModel implements Serializable{
    private int _userid;
    private int _orderid;
    private int credit;
    private int id;

    public int get_userid() {
        return _userid;
    }

    public void set_userid(int _userid) {
        this._userid = _userid;
    }

    public int get_orderid() {
        return _orderid;
    }

    public void set_orderid(int _orderid) {
        this._orderid = _orderid;
    }

    public int getCredit() {
        return credit;
    }

    public void setCredit(int credit) {
        this.credit = credit;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
