package com.kotaksarapan.kotaksarapan.model.Login;

/**
 * Created by wresniwahyu on 20/07/2017.
 */

public class LoginResponse {
    private boolean status;
    private String message;
    private UserModel user;
    private String token;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
