package com.kotaksarapan.kotaksarapan.model.orderitem;

/**
 * Created by wresniwahyu on 25/08/2017.
 */

public class OrderItemResponse {
    private String success;
    private String message;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
