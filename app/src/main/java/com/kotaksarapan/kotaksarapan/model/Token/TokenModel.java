package com.kotaksarapan.kotaksarapan.model.Token;

/**
 * Created by wresniwahyu on 28/07/2017.
 */

public class TokenModel {
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
