package com.kotaksarapan.kotaksarapan.model.Login;

import com.google.gson.annotations.SerializedName;
import com.kotaksarapan.kotaksarapan.model.Login.UserModel;

/**
 * Created by wresniwahyu on 19/07/2017.
 */

public class RegisterResponse {
    private boolean status;
    private String message;
    private UserModel user;
    private String token;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
