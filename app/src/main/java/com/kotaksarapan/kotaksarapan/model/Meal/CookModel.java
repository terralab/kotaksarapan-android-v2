package com.kotaksarapan.kotaksarapan.model.Meal;

/**
 * Created by wresniwahyu on 5/31/2017.
 */

public class CookModel {
    private String id;
    private String fullname;
    private String nickname;
    private String imagecook;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getImagecook() {
        return imagecook;
    }

    public void setImagecook(String imagecook) {
        this.imagecook = imagecook;
    }
}
