package com.kotaksarapan.kotaksarapan.model.ListMeals;

/**
 * Created by wresniwahyu on 11/07/2017.
 */

public class MetaModel {
    private PaginationModel pagination;

    public PaginationModel getPagination() {
        return pagination;
    }

    public void setPagination(PaginationModel pagination) {
        this.pagination = pagination;
    }
}
