package com.kotaksarapan.kotaksarapan.model.address;

/**
 * Created by wresniwahyu on 19/09/2017.
 */

public class AddressResponse {
    private DataModel data;

    public DataModel getData() {
        return data;
    }

    public void setData(DataModel data) {
        this.data = data;
    }
}
