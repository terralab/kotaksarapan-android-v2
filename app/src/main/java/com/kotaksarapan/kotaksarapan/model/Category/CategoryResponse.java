package com.kotaksarapan.kotaksarapan.model.Category;

import java.util.List;

/**
 * Created by wresniwahyu on 5/22/2017.
 */

public class CategoryResponse {
    private List<DataModel> data;

    public List<DataModel> getData() {
        return data;
    }

    public void setData(List<DataModel> data) {
        this.data = data;
    }
}
