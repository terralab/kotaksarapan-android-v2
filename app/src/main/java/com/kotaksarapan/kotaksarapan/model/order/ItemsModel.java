package com.kotaksarapan.kotaksarapan.model.order;

/**
 * Created by wresniwahyu on 25/09/2017.
 */

public class ItemsModel {
    private int id;
    private int amount;
    private String note;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
