package com.kotaksarapan.kotaksarapan.model.Category;

/**
 * Created by wresniwahyu on 5/29/2017.
 */

public class CategoryModel {
    private int id;
    private String title;

    public CategoryModel() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
