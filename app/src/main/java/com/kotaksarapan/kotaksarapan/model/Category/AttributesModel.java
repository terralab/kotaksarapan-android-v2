package com.kotaksarapan.kotaksarapan.model.Category;

/**
 * Created by wresniwahyu on 5/22/2017.
 */

public class AttributesModel {
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
