package com.kotaksarapan.kotaksarapan.model.cart;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by wresniwahyu on 4/2/2017.
 */

public class CartModel implements Parcelable{
    private String mealid;
    private String meal;
    private int price;
    private int amount;
    private int totalSide;



    public CartModel() {
    }

    protected CartModel(Parcel in) {
        meal = in.readString();
        price = in.readInt();
        amount = in.readInt();
        totalSide = in.readInt();
    }

    public static final Creator<CartModel> CREATOR = new Creator<CartModel>() {
        @Override
        public CartModel createFromParcel(Parcel in) {
            return new CartModel(in);
        }

        @Override
        public CartModel[] newArray(int size) {
            return new CartModel[size];
        }
    };

    public String getMealid() {
        return mealid;
    }

    public void setMealid(String mealid) {
        this.mealid = mealid;
    }

    public String getMeal() {
        return meal;
    }

    public void setMeal(String meal) {
        this.meal = meal;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getTotalSide() {
        return totalSide;
    }

    public void setTotalSide(int totalSide) {
        this.totalSide = totalSide;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(meal);
        parcel.writeInt(price);
        parcel.writeInt(amount);
        parcel.writeInt(totalSide);
    }
}
