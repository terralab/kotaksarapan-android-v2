package com.kotaksarapan.kotaksarapan.model.sqlite;

/**
 * Created by wresniwahyu on 24/08/2017.
 */

public class DeliveryAddressModel {
    private String fullname;
    private String location;
    private String street;
    private String message;

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
