package com.kotaksarapan.kotaksarapan.model.Meal;

/**
 * Created by wresniwahyu on 5/31/2017.
 */

public class AttributesModel {
    private String slug;
    private String title;
    private String imagesmall;
    private String summary;
    private String description;
    private Float price;
    private Float rating;
    private int categoryId;
    private int _cookid;
    private boolean isfavorite;
    private boolean isavailable;
    private CookModel cook;

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImagesmall() {
        return imagesmall;
    }

    public void setImagesmall(String imagesmall) {
        this.imagesmall = imagesmall;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int get_cookid() {
        return _cookid;
    }

    public void set_cookid(int _cookid) {
        this._cookid = _cookid;
    }

    public boolean isfavorite() {
        return isfavorite;
    }

    public void setIsfavorite(boolean isfavorite) {
        this.isfavorite = isfavorite;
    }

    public boolean isavailable() {
        return isavailable;
    }

    public void setIsavailable(boolean isavailable) {
        this.isavailable = isavailable;
    }

    public CookModel getCook() {
        return cook;
    }

    public void setCook(CookModel cook) {
        this.cook = cook;
    }
}
