package com.kotaksarapan.kotaksarapan.model.Login;

/**
 * Created by wresniwahyu on 15/08/2017.
 */

public class LoginProviderModel {
    private String email;
    private String provider;
    private String provider_id;
    private String name;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getProvider_id() {
        return provider_id;
    }

    public void setProvider_id(String provider_id) {
        this.provider_id = provider_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
