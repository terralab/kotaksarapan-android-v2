package com.kotaksarapan.kotaksarapan.model.topup;

/**
 * Created by wresniwahyu on 18/09/2017.
 */

public class TopUpResponse {
    private DataModel data;

    public DataModel getData() {
        return data;
    }

    public void setData(DataModel data) {
        this.data = data;
    }
}
