package com.kotaksarapan.kotaksarapan.model.side;

/**
 * Created by wresniwahyu on 14/09/2017.
 */

public class DataModel {
    private String type;
    private int id;
    private AttributesModel attributes;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public AttributesModel getAttributes() {
        return attributes;
    }

    public void setAttributes(AttributesModel attributes) {
        this.attributes = attributes;
    }
}
