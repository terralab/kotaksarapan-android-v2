package com.kotaksarapan.kotaksarapan.model.Login;

/**
 * Created by wresniwahyu on 19/07/2017.
 */

public class RegisterModel {
    private String email;
    private String password;
    private String name;
    private String phone;
    private String facebook_id;
    private String gplus_id;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFacebook_id() {
        return facebook_id;
    }

    public void setFacebook_id(String facebook_id) {
        this.facebook_id = facebook_id;
    }

    public String getGplus_id() {
        return gplus_id;
    }

    public void setGplus_id(String gplus_id) {
        this.gplus_id = gplus_id;
    }
}
