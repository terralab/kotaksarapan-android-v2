package com.kotaksarapan.kotaksarapan.model.Token;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.kotaksarapan.kotaksarapan.model.Login.*;

import java.io.Serializable;

/**
 * Created by wresniwahyu on 28/07/2017.
 */

public class TokenStatusResponse implements Serializable {
    private boolean isActive;
    private UserModel user;

     public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }
}
