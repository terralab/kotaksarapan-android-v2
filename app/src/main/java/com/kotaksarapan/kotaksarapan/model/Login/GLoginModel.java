package com.kotaksarapan.kotaksarapan.model.Login;

/**
 * Created by wresniwahyu on 20/07/2017.
 */

public class GLoginModel {
    private String email;
    private String name;
    private String g_token;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getG_token() {
        return g_token;
    }

    public void setG_token(String g_token) {
        this.g_token = g_token;
    }
}
