package com.kotaksarapan.kotaksarapan.model.sqlite;

/**
 * Created by wresniwahyu on 08/08/2017.
 */

public class OrderItemModel {
    private String cookid;
    private String mealid;
    private String mealname;
    private String price;
    private String originalprice;
    private String amount;
    private String orderid;
    private String note;

    public String getCookid() {
        return cookid;
    }

    public void setCookid(String cookid) {
        this.cookid = cookid;
    }

    public String getMealid() {
        return mealid;
    }

    public void setMealid(String mealid) {
        this.mealid = mealid;
    }

    public String getMealname() {
        return mealname;
    }

    public void setMealname(String mealname) {
        this.mealname = mealname;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getOriginalprice() {
        return originalprice;
    }

    public void setOriginalprice(String originalprice) {
        this.originalprice = originalprice;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
