package com.kotaksarapan.kotaksarapan.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.kotaksarapan.kotaksarapan.R;
import com.kotaksarapan.kotaksarapan.model.side.SideModel;

import java.util.List;

/**
 * Created by wresniwahyu on 14/09/2017.
 */

public class SideAdapter extends RecyclerView.Adapter<SideAdapter.ViewHolder> {
    private List<SideModel> sideList;
    private Context context;
    private TextView tv;

    public SideAdapter(List<SideModel> sideList, TextView tv, Context context) {
        this.sideList = sideList;
        this.context = context;
        this.tv = tv;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_side, parent, false);
        return new SideAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        SideModel side = sideList.get(position);

        holder.txtSide.setText(side.getSide());
        holder.txtPrice.setText(String.valueOf(side.getPrice()));
        holder.txtAmount.setText(String.valueOf(side.getAmount()));
    }

    @Override
    public int getItemCount() {
        return sideList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView txtSide;
        private TextView txtPrice;
        private TextView txtAmount;
        private ImageButton btnMinus;
        private ImageButton btnPlus;
        private int amount ;
        private int total;
        int price;

        public ViewHolder(View itemView) {
            super(itemView);
            txtSide = (TextView) itemView.findViewById(R.id.txtSide);
            txtPrice = (TextView) itemView.findViewById(R.id.txtPrice);
            txtAmount = (TextView) itemView.findViewById(R.id.txtAmount);
            btnMinus = (ImageButton) itemView.findViewById(R.id.btnMinus);
            btnPlus = (ImageButton) itemView.findViewById(R.id.btnPlus);

            btnMinus.setOnClickListener(this);
            btnPlus.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()){
                case R.id.btnMinus:

                    break;
                case R.id.btnPlus:

                    break;
            }
        }
    }
}
