package com.kotaksarapan.kotaksarapan.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

import com.kotaksarapan.kotaksarapan.MealsFragment;
import com.kotaksarapan.kotaksarapan.model.Category.CategoryModel;

import java.util.List;

/**
 * Created by wresniwahyu on 5/28/2017.
 */

public class CategoryPagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    Fragment fragment = null;
    private List<CategoryModel> category;

    public CategoryPagerAdapter(FragmentManager fm, int NumOfTabs, List<CategoryModel> category) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
        this.category = category;
    }


    @Override
    public Fragment getItem(int position) {
        for (int i = 0; i < mNumOfTabs ; i++) {
            if (i == position) {
                int id = category.get(position).getId();
                String title = category.get(position).getTitle();
                Log.i("TAG", "getItem: id "+id);
                Log.i("TAG", "getItem: title"+title);
                fragment = MealsFragment.newInstance(id, title);
                /*Bundle b = new Bundle();
                b.putString("id",category.get(position).getId());
                fragment.setArguments(b);*/
                break;
            }
        }
        return fragment;

    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }

    @Override
    public CharSequence getPageTitle(int position) {
//        return super.getPageTitle(position);
        return category.get(position).getTitle();
    }
}
