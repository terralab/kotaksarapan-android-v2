package com.kotaksarapan.kotaksarapan.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.kotaksarapan.kotaksarapan.HomecookProfileActivity;
import com.kotaksarapan.kotaksarapan.MealDetailActivity;
import com.kotaksarapan.kotaksarapan.R;
import com.kotaksarapan.kotaksarapan.interfaces.OnCartClick;
import com.kotaksarapan.kotaksarapan.model.Meal.MealModel;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by wresniwahyu on 08/02/2017.
 */

public class MealAdapter extends RecyclerView.Adapter<MealAdapter.ViewHolder> {
    private List<MealModel> listMeal;
    private Context context;
    private OnCartClick clickListener;
    private int globalPosition = -1;

    public MealAdapter(List<MealModel> listMeal, Context context, OnCartClick click) {
        this.listMeal = listMeal;
        this.context = context;
        clickListener = click;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_meals,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        //todo action here
        MealModel meals = listMeal.get(position);

        holder.mealId = meals.getId();
        holder.cookId = meals.get_cookid();
        holder.txtMeals.setText(meals.getTitle());
        holder.txtMeals.setPaintFlags(holder.txtMeals.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);
        holder.txtHomecookName.setText(meals.getNickname());
        holder.txtPrice.setText("Rp. "+meals.getPrice());
        holder.rtbMealsRating.setRating(meals.getRating()/2);
        holder.txtSummary.setText(meals.getSummary());

        Log.i("TAG", "onBindViewHolder: title ---> "+meals.getTitle()+" --> "+meals.getImagesmall());

        Glide.with(context).load(meals.getImagesmall()).centerCrop().placeholder(R.drawable.placeholder_food).dontAnimate().into(holder.imgMeals);
        Glide.with(context).load(meals.getImagecook()).placeholder(R.drawable.placeholder_user).dontAnimate().into(holder.imgHomecook);

        if(position == globalPosition && holder.layoutSummary.getVisibility() == View.GONE){
            holder.layoutSummary.setVisibility(View.VISIBLE);
        }else{
            holder.layoutSummary.setVisibility(View.GONE);
        }

        /*if (meals.getStatus().equals("promo")){
            holder.discount.setVisibility(View.VISIBLE);
        }else{
            holder.discount.setVisibility(View.GONE);
        }*/
    }

    @Override
    public int getItemCount() {
        return listMeal.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView imgMeals;
        private CircleImageView imgHomecook;
        private TextView txtMeals, txtHomecookName, txtPrice;
        private RatingBar rtbMealsRating;
        private ImageButton btnCart,btnRate;
        private RelativeLayout discount;
        private RelativeLayout layoutSummary;
        private TextView txtSummary, readmore;
        private int mealId;
        private int cookId;

        public ViewHolder(final View itemView) {
            super(itemView);

            imgMeals = (ImageView) itemView.findViewById(R.id.imgMeals);
            imgHomecook = (CircleImageView) itemView.findViewById(R.id.imgHomecook);
            txtMeals = (TextView) itemView.findViewById(R.id.txtMeals);
            txtHomecookName = (TextView) itemView.findViewById(R.id.txtHomecookName);
            txtPrice = (TextView) itemView.findViewById(R.id.txtPrice);
            rtbMealsRating = (RatingBar) itemView.findViewById(R.id.rtbMealsRating);
            btnCart = (ImageButton) itemView.findViewById(R.id.btnCart);
            btnRate = (ImageButton) itemView.findViewById(R.id.btnRate);
            discount = (RelativeLayout) itemView.findViewById(R.id.Discount);
            layoutSummary = (RelativeLayout) itemView.findViewById(R.id.layoutSummary);
            txtSummary = (TextView) itemView.findViewById(R.id.txtSummary);
            readmore = (TextView) itemView.findViewById(R.id.readmore);

            itemView.setOnClickListener(this);
            imgMeals.setOnClickListener(this);
            imgHomecook.setOnClickListener(this);
            readmore.setOnClickListener(this);
            btnCart.setOnClickListener(this);
            btnRate.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            switch (view.getId()){
                case R.id.imgMeals:
                    globalPosition = getAdapterPosition();
                    notifyDataSetChanged();
                    break;
                case R.id.imgHomecook:
                    Intent intentHomecook = new Intent(context, HomecookProfileActivity.class);
                    intentHomecook.putExtra("id",cookId);
                    context.startActivity(intentHomecook);
                    break;
                case R.id.readmore:
                    Intent intent = new Intent(context, MealDetailActivity.class);
                    intent.putExtra("id", mealId);
                    context.startActivity(intent);
                    break;
                case R.id.btnCart:
                    String cookID = String.valueOf(listMeal.get(getAdapterPosition()).get_cookid());
                    String mealID = String.valueOf(listMeal.get(getAdapterPosition()).getId());
                    String mealName = listMeal.get(getAdapterPosition()).getTitle();
                    int price = listMeal.get(getAdapterPosition()).getPrice();
                    int originalPrice = listMeal.get(getAdapterPosition()).getOriginalprice();
                    int amount = 1;
                    clickListener.onClickCart(cookID, mealID, mealName, price, originalPrice, amount);
                    break;
                case R.id.btnRate:
                    Toast.makeText(context,"rate selected",Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    }
}
