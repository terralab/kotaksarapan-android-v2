package com.kotaksarapan.kotaksarapan.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kotaksarapan.kotaksarapan.CartAddNoteActivity;
import com.kotaksarapan.kotaksarapan.CartAddSideActivity;
import com.kotaksarapan.kotaksarapan.interfaces.OnClickAmount;
import com.kotaksarapan.kotaksarapan.model.cart.CartModel;
import com.kotaksarapan.kotaksarapan.Helper.MySQLHelper;
import com.kotaksarapan.kotaksarapan.R;

import java.util.List;

/**
 * Created by wresniwahyu on 4/2/2017.
 */

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.ViewHolder>{
    private List<CartModel> cartList;
    private Context context;
    private TextView tv;
    private OnClickAmount listener;

    public CartAdapter(List<CartModel> cartList, TextView tv, Context context, OnClickAmount listener) {
        this.cartList = cartList;
        this.context = context;
        this.tv = (TextView) tv.findViewById(R.id.txtTotalPrice);
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_cart,parent,false);
        return new CartAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        CartModel cart = cartList.get(position);

        holder.price = cart.getPrice();

        holder.txtMeal.setText(cart.getMeal());
        holder.txtPrice.setText(String.valueOf(cart.getPrice()));
        holder.txtAmount.setText(String.valueOf(cart.getAmount()));

    }

    @Override
    public int getItemCount() {
        return cartList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener, View.OnClickListener {
        private TextView txtMeal, txtPrice, txtAmount, txtTotalSide;
        private RelativeLayout itemLayout, menuLayout;
        private Button btnDelete, btnSide, btnAddNote;
        private ImageButton btnMinus;
        private ImageButton btnPlus;
        private int amount ;
        private int total;
        int price;
        private MySQLHelper db;

        public ViewHolder(final View itemView) {
            super(itemView);
            txtMeal = (TextView) itemView.findViewById(R.id.txtMeal);
            txtPrice = (TextView) itemView.findViewById(R.id.txtPrice);
            txtAmount = (TextView) itemView.findViewById(R.id.txtAmount);
            txtTotalSide = (TextView) itemView.findViewById(R.id.txtTotalSide);

            itemLayout = (RelativeLayout) itemView.findViewById(R.id.itemLayout);
            menuLayout = (RelativeLayout) itemView.findViewById(R.id.menuLayout);
            btnDelete = (Button) itemView.findViewById(R.id.btnDelete);
            btnSide = (Button) itemView.findViewById(R.id.btnSide);
            btnAddNote = (Button) itemView.findViewById(R.id.btnAddNote);

            btnMinus = (ImageButton) itemView.findViewById(R.id.btnMinus);
            btnPlus = (ImageButton) itemView.findViewById(R.id.btnPlus);

            db = new MySQLHelper(context);

            itemLayout.setOnLongClickListener(this);
            btnDelete.setOnClickListener(this);
            btnAddNote.setOnClickListener(this);
            btnSide.setOnClickListener(this);
            btnMinus.setOnClickListener(this);
            btnPlus.setOnClickListener(this);
        }

        @Override
        public boolean onLongClick(View view) {
            menuLayout.setVisibility(View.VISIBLE);
            return true;
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()){
                case R.id.btnDelete:
                    db.removeItem(cartList.get(getAdapterPosition()).getMeal());
                    removeAt(getAdapterPosition());
                    listener.onClickItemAmount();
                    break;
                case R.id.btnSide:
                    menuLayout.setVisibility(View.GONE);
                    Intent intent = new Intent(context, CartAddSideActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                    break;
                case R.id.btnAddNote:
                    menuLayout.setVisibility(View.GONE);
                    Intent i = new Intent(context, CartAddNoteActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(i);
                    break;
                case R.id.btnMinus:
                    amount = Integer.parseInt(txtAmount.getText().toString());
                    if(amount > 1){
                        amount = amount - 1;
                        txtAmount.setText(""+amount);
                        cartList.get(getAdapterPosition()).setAmount(amount);

                        db.updateAmount(cartList.get(getAdapterPosition()).getMealid(),amount);
                        listener.onClickItemAmount();
                        /*total = Integer.parseInt(tv.getText().toString());
                        tv.setText(Integer.toString(total-price));*/
                    }
                    break;
                case R.id.btnPlus:
                    amount = Integer.parseInt(txtAmount.getText().toString());
                    if(amount < 99){
                        amount = amount + 1;
                        txtAmount.setText(""+amount);
                        cartList.get(getAdapterPosition()).setAmount(amount);

                        db.updateAmount(cartList.get(getAdapterPosition()).getMealid(),amount);
                        listener.onClickItemAmount();
                        /*total = Integer.parseInt(tv.getText().toString());
                        tv.setText(Integer.toString(total+price));*/
                    }
                    break;
            }
        }
    }

    public void removeAt(int position) {
        cartList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, cartList.size());
    }
}
