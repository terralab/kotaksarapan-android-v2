package com.kotaksarapan.kotaksarapan.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kotaksarapan.kotaksarapan.Helper.TopUpModel;
import com.kotaksarapan.kotaksarapan.R;
import com.kotaksarapan.kotaksarapan.interfaces.OnClickTopUp;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

/**
 * Created by wresniwahyu on 4/2/2017.
 */

public class TopUpAdapter extends RecyclerView.Adapter<TopUpAdapter.ViewHolder>{
    private List<TopUpModel> topupList;
    private Context context;
    private int globalPosition = -1;
    private OnClickTopUp clickListener;

    public TopUpAdapter(List<TopUpModel> topupList, Context context, OnClickTopUp click) {
        this.topupList = topupList;
        this.context = context;
        clickListener = click;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_topup,parent,false);
        return new TopUpAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        TopUpModel topup = topupList.get(position);

//        DecimalFormat rupiah = (DecimalFormat) DecimalFormat.getCurrencyInstance();
//        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();
//        formatRp.setCurrencySymbol("Rp. ");
//        rupiah.setDecimalFormatSymbols(formatRp);

        Locale localeID = new Locale("in","ID");
        NumberFormat rupiah = NumberFormat.getInstance(localeID);
        holder.txtAmount.setText("Rp. "+rupiah.format(topup.getAmount())+",-");

        if(position == globalPosition){
            holder.amountLayout.setBackgroundColor(0xfffec713);
        }else{
            holder.amountLayout.setBackgroundColor(0xfffbfbfb);
        }

    }

    @Override
    public int getItemCount() {
        return topupList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private RelativeLayout amountLayout;
        private TextView txtAmount;

        public ViewHolder(View itemView) {
            super(itemView);
            amountLayout = (RelativeLayout) itemView.findViewById(R.id.amountLayout);
            txtAmount = (TextView) itemView.findViewById(R.id.txtAmount);
            amountLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            clickListener.onClickCreditAmount(topupList.get(getAdapterPosition()).getAmount());
            globalPosition = getAdapterPosition();
            notifyDataSetChanged();
        }
    }
}
