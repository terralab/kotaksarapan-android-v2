package com.kotaksarapan.kotaksarapan.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.kotaksarapan.kotaksarapan.R;
import com.kotaksarapan.kotaksarapan.model.address.AddressModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wresniwahyu on 20/09/2017.
 */

public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.ViewHolder> {
    private List<AddressModel> listAddress;
    private Context context;

    public AddressAdapter(List<AddressModel> address, Context context) {
        this.listAddress = address;
        this.context = context;
    }

    public void swap(List<AddressModel> datas){
        if(datas == null || datas.size()==0)
            return;
        if (listAddress != null && listAddress.size()>0)
            listAddress.clear();
            listAddress.addAll(datas);
            notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_address,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        AddressModel address = listAddress.get(position);

        holder.txtLabel.setText(address.getLabel());
        holder.txtName.setText(address.getName());
        holder.txtAddress.setText(address.getAddress());
        holder.txtAddressExtra.setText(address.getAddress_extra());
    }

    @Override
    public int getItemCount() {
        return listAddress.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView txtLabel;
        private TextView txtName;
        private TextView txtAddress;
        private TextView txtAddressExtra;
        private Button btnPrimary;
        private Button btnEdit;
        private Button btnDelete;

        public ViewHolder(View itemView) {
            super(itemView);

            txtLabel = (TextView) itemView.findViewById(R.id.txtLabel);
            txtName = (TextView) itemView.findViewById(R.id.txtName);
            txtAddress = (TextView) itemView.findViewById(R.id.txtAddress);
            txtAddressExtra = (TextView) itemView.findViewById(R.id.txtAddressExtra);
            btnPrimary = (Button) itemView.findViewById(R.id.btnPrimary);
            btnEdit = (Button) itemView.findViewById(R.id.btnEdit);
            btnDelete = (Button) itemView.findViewById(R.id.btnDelete);

            btnPrimary.setOnClickListener(this);
            btnEdit.setOnClickListener(this);
            btnDelete.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()){
                case R.id.btnPrimary:

                    break;
                case R.id.btnEdit:

                    break;
                case R.id.btnDelete:

                    break;
            }
        }
    }
}
