package com.kotaksarapan.kotaksarapan;

import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;

public class LoadingActivity extends DialogFragment {
    private ImageView imageLoading;
    private Button btnDismiss;

    private AnimationDrawable animationDrawable;

    static LoadingActivity newInstance() {
        LoadingActivity f = new LoadingActivity();
        return f;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_loading, container, false);

        imageLoading = (ImageView) v.findViewById(R.id.imageLoading);
        btnDismiss = (Button) v.findViewById(R.id.btndismiss);

        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

//        Glide.with(this).load(R.drawable.breaky_monster).asGif().into(imageLoading);

//        ImageView progress = (ImageView)findViewById(R.id.progress_bar);
        if (imageLoading != null) {
            imageLoading.setVisibility(View.VISIBLE);
            AnimationDrawable frameAnimation = (AnimationDrawable)imageLoading.getDrawable();
            frameAnimation.setCallback(imageLoading);
            frameAnimation.setVisible(true, true);
            frameAnimation.start();
        }

        return v;
    }


    //todo : method buat manggil dialog fragment loading ini
    /*
    void showAnimationLoading() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        DialogFragment loading = LoadingActivity.newInstance();
        loading.show(ft, "dialog");
    }
    */


}
