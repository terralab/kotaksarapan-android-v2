package com.kotaksarapan.kotaksarapan;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.TextView;

import com.kotaksarapan.kotaksarapan.Helper.MySQLHelper;
import com.kotaksarapan.kotaksarapan.Helper.SessionManager;
import com.kotaksarapan.kotaksarapan.adapter.SideAdapter;
import com.kotaksarapan.kotaksarapan.model.side.DataModel;
import com.kotaksarapan.kotaksarapan.model.side.SideModel;
import com.kotaksarapan.kotaksarapan.model.side.SideResponse;
import com.kotaksarapan.kotaksarapan.rest.loader.SideLoader;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class CartAddSideActivity extends AppCompatActivity implements SideLoader.SideListener {
    private LinearLayoutManager manager;
    private RecyclerView rvSide;
    private SideAdapter adapter;
    private List<SideModel> sideList;
    
    private SideLoader loader;
    
    private SessionManager session;
    private MySQLHelper db;

    private TextView txtTotalPrice;

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart_add_side);
        
        session = new SessionManager(getApplicationContext());

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        txtTotalPrice = (TextView) findViewById(R.id.txtTotalPrice);

        sideList = new ArrayList<>();
        adapter = new SideAdapter(sideList, txtTotalPrice, getApplicationContext());
        rvSide = (RecyclerView) findViewById(R.id.rvSide);
        manager = new LinearLayoutManager(getApplicationContext());
        rvSide.setLayoutManager(manager);
        rvSide.setAdapter(adapter);
        
        loader = new SideLoader();
        loader.load(session.getAuthToken(), this);

    }

    @Override
    public void onComplete(SideResponse response) {
        Log.i("TAG", "onComplete: Side list");

        for(DataModel data : response.getData()){
            SideModel side = new SideModel();
            Log.i("TAG", "onComplete: side name --> "+data.getAttributes().getName());
            side.setSide(data.getAttributes().getName());
            side.setPrice(data.getAttributes().getPrice());
            sideList.add(side);
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onError(String m) {

    }
}
